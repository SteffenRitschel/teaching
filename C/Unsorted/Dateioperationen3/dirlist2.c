#include <sys/types.h>
#include <dirent.h>
#include <stdio.h>

int main(int argc, char **argv)
{
    int run = 1;
    char buffer[100];
    DIR *dirHandle;
    struct dirent * dirEntry;

    dirHandle = opendir(".");
    if (dirHandle) {
       while (0 != (dirEntry = readdir(dirHandle))) {
          printf("%d) %s\n", run, dirEntry->d_name);
          run ++;
       }
       closedir(dirHandle);
    }
}
