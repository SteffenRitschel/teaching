/**********************************************************************/
/* Disketten-Utility: DUMP <filename> */
/* Das Programm zeigt den Inhalt einer Datei in ASCII-Darstellung */
/* und hexadezimal an. */
/* Demonstration: wahlfreier Dateizugriff mit fseek(..) */
/**********************************************************************/
#include <stdio.h>
#include <ctype.h> /* enthält Funktion isprint(..) */
#include <stdlib.h> /* enthält Funktion exit(..) */
#define SEKTORSIZE 128
#define LESEN "rb"
void Anzeige(int liessekt); /* Prototyp der Funktion */
char buffer[SEKTORSIZE]; /* globale Variablendeklaration */

int main(int argc, char *argv[])
{
	FILE *in, *out;
	int sektor, liessekt;
	if (argc != 2) /* argc gibt Anzahl der übergebenen Parameter an */
	{
		printf("übergebene Parameter sind nicht vollständig !");
		exit(1); /* gibt an die MS-DOS Umgebung den Wert 1 zurück */
	}

	if((fp=fopen(argv[1], LESEN))==NULL) /* übergebene Datei wird geöffnet */
	{
		printf("\nSorry, ich kann die Datei %s nicht öffnen !", argv[1]);
		exit(1);
	}
	do
	{
		printf("Gib Sektor-Nr. : ");
		scanf("%1d", &sektor);
		if (fseek(fp,sektor*SIZE,SEEK_SET)) /* SEEK_SET = Konstante für Dateianfang */
			printf(" SEEK Error !\n"); /* positioniere Dateizeiger */

		if ((liessekt= fread(buffer,1,SIZE,fp)) != SIZE)
			printf(" Dateiende erreicht !\n"); /* Lesezugriff */
		
		Anzeige(liessekt);
	} while(sektor>=0);

} /* Ende Hauptfunktion */

void Anzeige(int sektnr)
/************* Zeigt einen Sektor auf dem Bildschirm an **************/
{
	int i,j;
	for (i=0; i<=sektnr/16; i++)
	{
		for (j=0; j<16; j++) printf("%3X", buffer[i*16+j];
		printf(" ");
		
		for (j=0; j<16; j++)
		{
			if (isprint(buffer[i*16+j])) printf("%c", buffer[i*16+j]);
			else print(".");
		}
		printf("\n");
	}
} 
