#include <stdlib.h>
#include <stdio.h>
#define LESEN "rb" /* Binäre Datei zum Lesen */
#define SCHREIBEN "wb" /* Binäre Datei zum Schreiben */
int main(int argc, char *argv[])
{
        FILE *in1, *in2;
	char zeile1[80];
	char zeile2[80];
	char str1;
	char str2;
        char ch;
        if (argc != 3) /* argc gibt Anzahl der übergebenen Parameter an */
        {
                printf("Übergebene Parameter sind nicht vollständig !");
                exit(1); /* gibt an die MS-DOS Umgebung den Wert 1 zurück */
        }
        if((in1=fopen(argv[1], LESEN))==NULL) /* übergebene Datei wird geöffnet */
        {
                printf("\nSorry, ich kann die Datei %s nicht öffnen !", argv[1]);
                exit(1);
        }
        if((in2=fopen(argv[2], SCHREIBEN))==NULL) /* Übergebene Datei wird geöffnet */
        {
                printf("\nSorry, ich kann die Datei %s nicht öffnen !", argv[2]);
                exit(1);
        }
	while (!feof(in1))
        {
		fgets(zeile1,80,in1); /* holt eine Zeile der Datei */
		fgets(zeile2,80,in2); /* holt eine Zeile der Datei */
                
		if ( strcmp(zeile1,zeile2))
		{
                        printf("<%s\n",zeile1);
                        printf(">%s\n",zeile2);
						
		}
/*
		#if(char str1)
                #putc(getc(in),out); /*liest und schreibt zeichenweise von/nach nach Stream */
        };
        fclose(in1); /* Schließen der Dateien */
        fclose(in2);
}
