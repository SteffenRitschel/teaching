/**********************************************************************/
/* Programm zum Kopieren von Dateien */
/* Quell- und Zieldatei müssen in der Kommandozeile angegeben werden */
/* Die Übertragung erfolgt Zeichen für Zeichen */
/* Aufruf von MS-DOS: KOPIERE config.sys config.bak */
/**********************************************************************/
#include <stdio.h>
#define LESEN "rb" /* Binäre Datei zum Lesen */
#define SCHREIBEN "wb" /* Binäre Datei zum Schreiben */
int main(int argc, char *argv[])
{
	FILE *in, *out;
	char ch;
	if (argc != 3) /* argc gibt Anzahl der übergebenen Parameter an */
	{
		printf("Übergebene Parameter sind nicht vollständig !");
		exit(1); /* gibt an die MS-DOS Umgebung den Wert 1 zurück */
	}
	if((in=fopen(argv[1], LESEN))==NULL) /* übergebene Datei wird geöffnet */
	{
		printf("\nSorry, ich kann die Datei %s nicht öffnen !", argv[1]);
		exit(1);
	}
	if((out=fopen(argv[2], SCHREIBEN))==NULL) /* Übergebene Datei wird geöffnet */
	{
		printf("\nSorry, ich kann die Datei %s nicht öffnen !", argv[2]);
		exit(1);
	}
	while (!feof(in))
	{
		putc(getc(in),out); /*liest und schreibt zeichenweise von/nach nach Stream */
	};
	fclose(in); /* Schließen der Dateien */
	fclose(out);
} 
