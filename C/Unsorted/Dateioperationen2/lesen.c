/**********************************************************************/
/* Liest eine Datei und gibt sie zeilenweise auf dem Monitor aus. */
/* Demonstration der Funktion fgets(..) */
/**********************************************************************/
#include <stdio.h>
int main()
{ 
	FILE *fp;
	char zeile[80], filename[25];
	char *str; /* str= Zeigervariable auf ein Zeichen */
	printf("Geben Sie den Dateinamen ein -> ");
	scanf("%s", filename); /* liest den Dateinamen ein */
	fp=fopen(filename,"r"); /* öffnet Textdatei zum Lesen */
	do
	{
		str = fgets(zeile,80,fp); /* holt eine Zeile der Datei */
		if (str != NULL)
			printf("%s\n",zeile); /* stellt eine Zeile auf Monitor dar */
	} while (str != NULL); /* wiederhole bis Dateiende (->NULL) erreicht */
	fclose(fp);
} 
