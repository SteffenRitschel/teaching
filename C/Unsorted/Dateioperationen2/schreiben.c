/**********************************************************************/
/* Das Programm schreibt den über Tastatur eingegebenen Text in eine */
/* Datei. Zieldatei muss in der Kommandozeile angegeben werden, */
/* z.B. auto.bat. Aufruf von MS-DOS: EDIT auto.bat */
/**********************************************************************/
#include <stdio.h>
#include <stdlib.h>
int main(int argc, char *argv[])
{
	FILE *filepointer;
	char ch;
	if (argc != 2) /* argc gibt die Anzahl der übergebenen Parameter an */
	{
		printf("Es wurde keine Zieldatei angegeben !");
		exit(1); /* gibt an die MS-DOS Umgebung den Wert 1 zurück */
	}
	if((filepointer=fopen(argv[1], "w"))==NULL) /* übergebene Datei wird geöffnet*/
	{
		printf("\nSorry, ich kann die Datei %s nicht öffnen !", argv[1]);
		exit(1);
	}
	do
	{
		ch=getchar(); /* Liest ein zeichen von Standardgerät stdin */
		putc(ch,filepointer); /* Schreibt zeichenweise nach Stream */
	} while (ch !='$');
	fclose(filepointer); /* Schließen der Datei */
} 
