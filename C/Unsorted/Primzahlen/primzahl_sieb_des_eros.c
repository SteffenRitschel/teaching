#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#define max_prim 179424673
int count=10000000;
//104800 1299709 15485863 179424673
//10000 100000 1000000 10000000
bool pr_feld[(max_prim+1)];  //Array der zu überprüfenden Zahlen
int number = 1;

int main(void)
{
        for(int i = 0; i <= max_prim; i++)
        {
                   pr_feld[i] = false;
        }

        int i = 2;
        //Sieb des Eratosthenes
        while(number<=count)
        {
                i++;
                if(!pr_feld[i])
                {
                        printf("\n%i %4d\t",number,i);
                        number++;
                        for(int j = 1;(j*i) <= max_prim;++j)
                        {
                                if((j*i) <= max_prim)
                                        pr_feld[(j*i)] = true;
                        }
                }
        }

        return EXIT_SUCCESS;
}
