#include <stdio.h>
#include <string.h>
    
/* Die Adressen-Datenstruktur */
typedef struct _adresse
{
	char name[100];
      	int plz; /* Postleitzahl */
} adresse;

/* Erzeuge ein Adressen-Record */
void mache_adresse (adresse *a, const char *name, const int plz)
{
      strncpy (a->name, name, 100);
      a->plz = plz;
}

int main (void)
{
      FILE *datei;
      adresse addr;

/* Datei erzeugen im Binärmodus, ansonsten kann es Probleme 
  	unter Windows geben, siehe Anmerkungen bei '''fopen()''' */

	datei = fopen ("testdatei.dat", "w");

	if (datei != NULL)
	{
	          mache_adresse (&addr, "Erika Mustermann", 12345);
        	  fwrite (&addr, sizeof (adresse), 1, datei);
	          mache_adresse (&addr, "Hans Müller", 54321);
        	  fwrite (&addr, sizeof (adresse), 1, datei);
	          mache_adresse (&addr, "Secret Services", 700);
        	  fwrite (&addr, sizeof (adresse), 1, datei);
	          mache_adresse (&addr, "Peter Mustermann", 12345);
        	  fwrite (&addr, sizeof (adresse), 1, datei);
	          mache_adresse (&addr, "Wikibook Nutzer", 99999);
        	  fwrite (&addr, sizeof (adresse), 1, datei);
	          fclose (datei);
	}

/* Datei zum Lesen öffnen - Binärmodus */
	datei = fopen ("testdatei.dat", "r");

	if (datei != NULL)
        {
          /* Hole den 4. Datensatz */
         	fseek(datei, 3 * sizeof (adresse), SEEK_SET);
          	fread (&addr, sizeof (adresse), 1, datei);
          	printf ("Name: %s (%d)\n", addr.name, addr.plz);
          	fclose (datei);
        }

      	return 0;

}


