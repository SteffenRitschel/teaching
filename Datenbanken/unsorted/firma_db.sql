connect reset;
Connect TO FIRMA_DB;
drop table firma;
CREATE TABLE Firma
	(FNR	SMALLINT NOT NULL,
	 Name	VARCHAR(35),
	 Sitz	VARCHAR(3),
	 Gesamt	INTEGER,
	 PRIMARY KEY(FNR));

INSERT INTO Firma VALUES
	(1341,'Sepp Hugendubel','M',NULL),
	(2200,'Kinderglück GmbH & Co. KG','HST',NULL),
	(5504,'Dr. Krach Didaktik-Spiele','F',NULL),
	(4102,'Qualle-Versand','HH',NULL),
	(1467,'Ottis Super Service International','HRO',NULL),
	(2262,'K.Waerner-Spiele','HRO',NULL),
	(1146,'game over','B',NULL),
	(3786,'Gemischtwaren AG',NULL,NULL);

