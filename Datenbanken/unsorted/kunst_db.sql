create table maler
	(KNR varchar(3) not null,
	 Name varchar(20),
	 Geburt smallint,
	 Geb_ort varchar(15),
	 Tod smallint,
	 primary key (KNR));
insert into maler values
	('K1','Lorrain',1734,'Paris',1812),
	('K2','Endler',1931,'M�nchen',NULL),
	('K3','Veccio',1480,'Paris',1528),
	('K4','Runge',1777,'Berlin',1810),
	('K5','van Goyen',1922,'Delft',NULL),
	('K6','Davis',1940,'London',NULL),
	('K7','Huysman',1822,'Delft',1925);
create table museum
	(ENR varchar(3) not null,
	 Bezeichnung varchar(20),
	 Sitz varchar(15),
	 primary key (ENR));
insert into museum values
	('E1','Louvre','Paris'),
	('E3','Nationalmuseum','Kopenhagen'),
	('E4','Deutsches Museum','Berlin');
create table galerie
	(ENR varchar(3) not null,
	 Bezeichnung varchar(20),
	 Sitz varchar(15),
	 primary key (ENR));
insert into galerie values
	('E2','National Galery','London'),
	('E5','Le Corbet','Paris'),
	('E6','Galerie Villon','Paris'),
	('E7','Nationalgalerie','Berlin');
create table bild
	(BNR varchar(3) not null,
	 Titel varchar(30),
	 Jahr smallint,
	 Wert smallint,
	 KNR varchar(3),
	 ENR varchar(3),
	 primary key (BNR));
insert into bild values
	('B1','Landschaft mit Pfeilen',1965,150,'K2','E2'),
	('B2','Wasserlauf',1946,200,'K5','E6'),
	('B3','Maria am Berge',1803,550,'K4','E1'),
	('B4','Dame mit H�ndchen',1784,1200,'K1','E5'),
	('B5','Bildnis Dirk van Bongen',1877,800,'K7','E1'),
	('B6','Verk�ndigung der Maria',1788,1400,'K1','E1'),
	('B7','Peng!',1996,120,'K2','E6'),
	('B8','Maria Magdalene',1422,1000,NULL,'E5'),
	('B9','Landschaft ohne Pfeile',1965,100,'K2','E2'),
	('B10','Bildnis des Dr. Franke',1977,200,'K5','E6'),
	('B11','Hinter der Bergen',1792,750,'K4','E1'),
	('B12','Auferstehung',1572,800,NULL,'E4'),
	('B13','Dorfszene bei Gewitter',1888,450,'K7','E1'),
	('B14','Das Fr�hst�ck',1756,800,'K1','E5'),
	('B15','Ansicht vom Gendarmenmarkt',1805,450,'K4','E7'),
	('B16','Portrait der Marte F.',1910,600,'K7','E7');
create table ausstellung
	(ANR varchar(3) not null,
	 A_Titel varchar(35),
	 Beginn smallint,
	 ENR varchar(3),
	 BNR varchar(3) not null,
	 primary key (ANR,BNR));
insert into ausstellung values
	('A1','Portraits des 18. Jahrhunderts',1997,'E3','B4'),
	('A1','Portraits des 18. Jahrhunderts',1997,'E3','B14'),
	('A2','Poststrukturalismus',1994,'E7','B1'),
	('A2','Poststrukturalismus',1994,'E7','B2'),
	('A2','Poststrukturalismus',1994,'E7','B7'),
	('A2','Poststrukturalismus',1994,'E7','B9'),
	('A3','Aufbruch der Maria',1997,'E1','B3'),
	('A3','Aufbruch der Maria',1997,'E1','B6'),
	('A3','Aufbruch der Maria',1997,'E1','B8'),
	('A3','Aufbruch der Maria',1997,'E1','B12'),
	('A4','Peng!',1997,'E1','B1'),
	('A4','Peng!',1997,'E1','B7'),
	('A4','Peng!',1997,'E1','B9'),
	('A5','Portraitmalerei',1992,'E2','B4'),
	('A5','Portraitmalerei',1992,'E2','B5'),
	('A5','Portraitmalerei',1992,'E2','B10'),
	('A5','Portraitmalerei',1992,'E2','B16'),
	('A6','Landschft und Stille',1992,'E2','B1'),
	('A6','Landschft und Stille',1992,'E2','B2'),
	('A6','Landschft und Stille',1992,'E2','B9'),
	('A6','Landschft und Stille',1992,'E2','B11'),
	('A6','Landschft und Stille',1992,'E2','B13'),
	('A7','Sammlung Sprengler',1999,'E1','B3'),
	('A7','Sammlung Sprengler',1999,'E1','B5'),
	('A7','Sammlung Sprengler',1999,'E1','B13'),
	('A8','Farbkl�nge',1990,'E6','B2'),
	('A8','Farbkl�nge',1990,'E6','B7'),
	('A8','Farbkl�nge',1990,'E6','B10');

