CONNECT TO TEST_DB;
DROP TABLE Mitarbeiter;
CREATE TABLE Mitarbeiter
(MNR	VARCHAR(3),
 Name	VARCHAR(20),
 Geburt	DATE, 
 Chef	VARCHAR(3));
INSERT INTO Mitarbeiter VALUES ('M1','Grabow','1944-03-11',NULL);
INSERT INTO Mitarbeiter VALUES ('M2','Jensen','1950-12-12','M1');
INSERT INTO Mitarbeiter VALUES ('M3','Meier','1954-01-17','M1');
INSERT INTO Mitarbeiter VALUES ('M4','Lindemann','1970-08-30','M3');
INSERT INTO Mitarbeiter VALUES ('M5','Schulz','1971-12-12','M3');
INSERT INTO Mitarbeiter VALUES ('M6','Kramer','1973-01-06','M3');
INSERT INTO Mitarbeiter VALUES ('M7','Otto','1972-09-21','M6');
INSERT INTO Mitarbeiter VALUES ('M8','Neumann','1972-03-01','M6');
INSERT INTO Mitarbeiter VALUES ('M9','Scholl','1980-12-17',NULL);
INSERT INTO Mitarbeiter VALUES ('M10','Knecht','1972-12-01','M9');
INSERT INTO Mitarbeiter VALUES ('M11','Pfordte','1982-04-07','M10');
INSERT INTO Mitarbeiter VALUES ('M12','Wulff','1979-03-03','M10');