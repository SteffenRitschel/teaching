1a)

SELECT A_Titel;
FROM Ausstellung;
WHERE Beginn>1996 AND Begin<2000;

1b)

SELECT A_Titel;
FROM Ausstellung;
WHERE Beginn BETWEEN 1997 AND 1999;

1c)

SELECT A_Titel;
FROM Ausstellung;
WHERE Beginn IN (1997,1998,1999);


2)

SELECT Name,Geburt ;
FROM Maler;
WHERE (Tod-Geburt>59) OR (Geburt<1950 AND TOD IS NULL);

3)

SELECT Name;
FROM Maler;
Where (Geburt BETWEEN 1500 AND 1600) OR (TOD BETWEEN 1500 AND 1600) AND (Geb_ORT IS ('Paris','London'));

4a)

SELECT COUNT(*)  AS Anzahl;
FROM Maler;
WHERE TOD IS NOT NULL;

4b)

SELECT COUNT(Tod)  AS Anzahl;
FROM Maler;

5a)

SELECT AVG(Tod-Geburt) AS Durchschnittsalter;
FROM Maler;
WHERE Tod IS NOT NULL;

5b)

SELECT SUM(Tod-Geburt)/COUNT(*) AS Durchschnittsalter;
FROM Maler;
WHERE Tod IS NOT NULL;
