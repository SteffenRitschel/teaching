﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace WindowsFormsApplication1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void FormKeyPressed(object sender, KeyPressEventArgs e)
        {

            if (e.KeyChar >= 65 && e.KeyChar <= 92)
            {
                MessageBox.Show("Form.KeyPress: '" +
                    e.KeyChar.ToString() + "' pressed.");

                switch (e.KeyChar)
                {
                    case (char)65:
                    case (char)66:
                    case (char)67:
                //        MessageBox.Show("Form.KeyPress: '" +
                //            e.KeyChar.ToString() + "' consumed.");
                        e.Handled = true;
                        break;
                }

                label1.Text = label1.Text + e.KeyChar.ToString();
            }
        }
    }
}
