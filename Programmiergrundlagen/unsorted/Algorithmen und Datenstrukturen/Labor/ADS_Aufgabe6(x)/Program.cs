﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace ADS_Aufgabe6
{

    class Einrichtung
    {
        public String Name, Straße, Ort;
        public int PLZ, Bettenzahl;
    }

    class Daten
    {
        string Dateiname = "../../KHR95_red.txt";
       // List<Einrichtung> Liste = new List<Einrichtung>();
        public string Dateiinhalt = "";
        public int Anzahl=0;

        public List<Einrichtung> read_data()
        {
            List<Einrichtung> Liste = new List<Einrichtung>();
            StreamReader tw = new StreamReader(Dateiname, System.Text.Encoding.Default);
            if (File.Exists(Dateiname))
            {
                Dateiinhalt = tw.ReadLine();    //Kopfzeile überspringen
                Dateiinhalt = tw.ReadLine();     //erste Zeile lesen
                Anzahl++;
                while (Dateiinhalt != null)
                {
                    int index = 0, index2 = 0;
                    Einrichtung einrichtung = new Einrichtung();

                    for (int i = 0; i < 5; i++)
                    {
                       
                            switch (i)  //Zeile der Datei durchgehen und Werte rausholen
                            {
                                case 0: einrichtung.Name = Dateiinhalt.Substring(index, index2 = Dateiinhalt.IndexOf("\t", index));
                                    break;
                                case 1:
                                    einrichtung.Straße = Dateiinhalt.Substring(index, index2 = Dateiinhalt.IndexOf("\t", index) - index);
                                    break;
                                case 2: einrichtung.PLZ = Convert.ToInt32(Dateiinhalt.Substring(index, index2 = Dateiinhalt.IndexOf("\t", index) - index));
                                    break;
                                case 3: einrichtung.Ort = Dateiinhalt.Substring(index, index2 = Dateiinhalt.IndexOf("\t", index) - index);
                                    break;
                                case 4: einrichtung.Bettenzahl = Convert.ToInt32(Dateiinhalt.Substring(index, index2 = Dateiinhalt.Length - index));
                                    break;
                            }
                        
                        index = Dateiinhalt.IndexOf("\t", index);   //Zum nächsten <TAB> gehen und index holen
                        index++;                                    //Index um 1 erhöhen damit er nicht auf den <TAB> ist
                    }
                    Liste.Add(einrichtung);                         //EInrichtung zur Liste hinzufügen
                    Dateiinhalt = tw.ReadLine();                    //nächste Zeile lesen
                    Anzahl++;
                }
            }
             tw.Close();
             return Liste;                                          //Liste mit Einrichtungen zurückgeben
        }
        public void write_data(List<Einrichtung> Liste)
        {
            {
                TextWriter tw = new StreamWriter("../../KHR95.txt", false, Encoding.Default); //false damit er nicht am ende Anhängt sonder 
                                                                                    //Encoding default damit er Sonderzeichen schreibt
              
               tw.WriteLine("Name"+"\t"+"StrHausNr"+"\t"+"PLZ"+"\t"+"Ort"+"\t"+"Bettenanzahl");             // in erste Zeile Tabellenheader schreiben
               for(int i=0;Liste.Count>i;i++)                                       // Liste in Datei schreiben
                {
                    Einrichtung einrichtung = Liste.ElementAt(i);
                    tw.WriteLine(einrichtung.Name + "\t" + einrichtung.Straße + "\t" + einrichtung.PLZ + "\t" + einrichtung.Ort + "\t"+einrichtung.Bettenzahl);
                }
                tw.Close();
            }
        }

        public void Ausgabe()
        {
            int Eingabe = 0;
            StreamReader tw = new StreamReader(Dateiname, System.Text.Encoding.Default);
               
            System.Console.WriteLine("Es gibt " + Anzahl + " Records.");
            System.Console.Write("Gib ein Record an das ausgegeben werden soll: ");
            Eingabe = Convert.ToInt32(System.Console.ReadLine());
            for (int i = 0; i < Eingabe; i++)
            {
                 if (File.Exists(Dateiname))
                {
                   tw.ReadLine();
                }
            }
            System.Console.WriteLine(tw.ReadLine());
        }

//------------------------------------------------Ab hier Aufgabe 7 mit den Sortierungen----------------------------------------------------------------;
        public List<Einrichtung>  Selection_sort()
        {
            List<Einrichtung> Liste =read_data();       //Liste holen
            List<Einrichtung> sortierte_Liste = new List<Einrichtung>();
            List<Einrichtung> temp_Liste = new List<Einrichtung>();
            temp_Liste.AddRange(Liste);     
            int zähler = 0;
            
            bool unsort=true;
            String Sortierung;
            
            System.Console.Write("Nach was soll sortiert werden: ");      
            Sortierung = System.Console.ReadLine();
            int vergleich = 0;
            int Aufwand = 0;
            switch (Sortierung)   //nach was soll sortiert werden 
            {                     //nach Aufgabenstellung sind nicht alle Sortierungen notwendig
                case "Name":
                    while (unsort==true)    //solange nicht gesamte Datei sortiert wurde
                    {
                        int temp = 0;
                        Sortierung = temp_Liste.ElementAt(0).Name.ToLower();    //nim ersten Namen in der Liste
                        for (int i = 1; temp_Liste.Count > i; i++)              //geh jeden Namen in der unsortierten Liste durch
                        {
                            Aufwand++;                                          //erhöhr den Aufwand bei jedem durchlauf um 1
                            int sort = Sortierung.CompareTo(temp_Liste.ElementAt(i).Name.ToLower());    //Vergleiche
                            if(sort==1)     //0=sind gleich; 1= ist kleiner; -1= ist größer
                            {
                                temp = i;
                                Sortierung = temp_Liste.ElementAt(i).Name.ToLower();    //weiter mit dem Vergleichen
                            }
                        }
                        sortierte_Liste.Add(Liste.ElementAt(temp));     //Einrichtung in sortierte Liste eintragen
                        temp_Liste.RemoveAt(temp);                      //eingetragene Einrichtung in unsortierte Liste löschen
                        if (temp_Liste.Count == 0)                      //wenn unsortierte Liste leer Sortiervorgang fertig
                            unsort = false;
                    }
                    break;      //alle anderen laufen gleich ab unterschied nur nach was sortiert wird
                   
                    case "Straße":
                    while (unsort == true)
                    {
                        int temp = 0;
                        Sortierung = temp_Liste.ElementAt(0).Straße.ToLower();
                        for (int i = 1; Liste.Count > i; i++)
                        {
                            Aufwand++;
                            int sort = Sortierung.CompareTo(temp_Liste.ElementAt(i).Straße.ToLower());
                            if (sort == 1)
                            {
                                temp = i;
                                Sortierung = temp_Liste.ElementAt(i).Straße.ToLower();
                            }
                        }
                        sortierte_Liste.Add(temp_Liste.ElementAt(temp));
                        temp_Liste.RemoveAt(temp);
                        if (temp_Liste.Count == 0)
                            unsort = false;
                    }
                    break;

                    case "Postleitzahl":
                    while (unsort == true)
                    {
                        int temp = 0;

                        for (int i = 1; temp_Liste.Count > i; i++)
                        {
                            Aufwand++;
                            if (temp_Liste.ElementAt(i).PLZ < temp_Liste.ElementAt(temp).PLZ)   //Vergleich
                            {
                                temp = i;
                                vergleich = temp_Liste.ElementAt(i).PLZ;
                            }
                        }
                        sortierte_Liste.Add(temp_Liste.ElementAt(temp));
                        temp_Liste.RemoveAt(temp);
                        if (temp_Liste.Count == 0)
                            unsort = false;
                    }
                    break;

                    case "Ort":
                    while (unsort == true)
                    {
                        int temp = 0;
                        Sortierung = temp_Liste.ElementAt(0).Ort.ToLower();
                        for (int i = 1; temp_Liste.Count > i; i++)
                        {
                            Aufwand++;
                            int sort = Sortierung.CompareTo(temp_Liste.ElementAt(i).Ort.ToLower());
                            if (sort == 1)
                            {
                                temp = i;
                                Sortierung = temp_Liste.ElementAt(i).Ort.ToLower();
                            }
                        }
                        sortierte_Liste.Add(temp_Liste.ElementAt(temp));
                        temp_Liste.RemoveAt(temp);
                        if (temp_Liste.Count == 0)
                            unsort = false;
                    }
                    break;

                    case "Bettenanzahl":
                    while (unsort == true)
                    {
                        int temp = 0;
                        for (int i = 1; temp_Liste.Count > i; i++)
                        {
                            Aufwand++;
                            if (temp_Liste.ElementAt(i).Bettenzahl < temp_Liste.ElementAt(temp).Bettenzahl)
                            {
                                temp = i;
                                vergleich = temp_Liste.ElementAt(i).Bettenzahl;
                            }
                        }
                        sortierte_Liste.Add(temp_Liste.ElementAt(temp));
                        temp_Liste.RemoveAt(temp);
                        if (temp_Liste.Count == 0)
                            unsort = false;
                    }
                    break;
            }
            System.Console.WriteLine("Aufwand= "+Aufwand);
            write_data(sortierte_Liste);
            return sortierte_Liste;
      }
       

        //----------------------------------------------------------------------------------------------------------------------------------------------
  
         public List<Einrichtung> insertion_sort()
         {
             List<Einrichtung> Liste = read_data();
             List<Einrichtung> temp_Liste = new List<Einrichtung>();
             temp_Liste.AddRange(Liste);
             List<Einrichtung> sortierte_Liste = new List<Einrichtung>();
             bool unsort = true;
             bool insert = false;
             String Sortierung;

             System.Console.Write("Nach was soll sortiert werden: ");
             Sortierung = System.Console.ReadLine();
             int vergleich = 0;
             int Aufwand = 0;
             int tausch = 0;
             switch (Sortierung)        //nach was soll sortiert werden
             {
                 case "Name":
                     sortierte_Liste.Add(Liste.ElementAt(0));   //ersten eintrag in sortierte Liste eintragen
                     temp_Liste.RemoveAt(0);                    //Eintrag aus unsortierte löschen
                     while (unsort == true)                     //solange nicht vollständigt sortiert wurde
                     {
                         insert = false;
                         int temp = 0;
                         if(temp_Liste.Count!=0)
                         for (int i = 0; sortierte_Liste.Count > i; i++)    //alle Einträge in sortierte Liste durchgehen durchgehen
                         {
                             Aufwand++;
                             int sort = temp_Liste.ElementAt(0).Name.ToLower().CompareTo(sortierte_Liste.ElementAt(i).Name.ToLower());
                             //vergleich Eintrag aus unsortierter Liste mit allen aus sortierter Liste


                             if (sort == -1)     //füge in sortierte Liste Eintrag ein an der Stelle 
                             {
                                 sortierte_Liste.Insert(i, temp_Liste.ElementAt(0));
                                 temp_Liste.RemoveAt(0);
                                 insert = true;
                                 if (temp_Liste.Count == 0)     //wenn sortierug fertig Aufwands Ausgabe und Rückgabe der sortierten Liste
                                 {
                                     System.Console.WriteLine("Aufwand= " + Aufwand);
                                     return sortierte_Liste;
                                 }
                             }
                         }
                         if (insert == false && temp_Liste.Count!=0)        //wenn nichts größeres gefunden wurde dann füge am ende an
                         {
                             sortierte_Liste.Add(Liste.ElementAt(0));
                             temp_Liste.RemoveAt(0);
                         }
                         if (temp_Liste.Count == 0)
                             unsort = false;
                     }
                     break;     //Ablauf bei allen anderen gleich
               
                 case "Straße":
                     sortierte_Liste.Add(Liste.ElementAt(0));
                     temp_Liste.RemoveAt(0);
                     while (unsort == true)     
                     {
                         insert = false;
                         int temp = 0;
                         for (int i = 0; sortierte_Liste.Count > i; i++)
                         {
                             Aufwand++;
                             int sort = temp_Liste.ElementAt(0).Straße.ToLower().CompareTo(sortierte_Liste.ElementAt(i).Straße.ToLower());
                             if (sort == -1)
                             {
                                 sortierte_Liste.Insert(i, temp_Liste.ElementAt(0));       
                                 temp_Liste.RemoveAt(0);
                                 insert = true;
                                 if (temp_Liste.Count == 0)     
                                 {
                                     System.Console.WriteLine("Aufwand= " + Aufwand);
                                     return sortierte_Liste;
                                 }
                             }
                         }
                         if (insert == false)      
                         {
                             sortierte_Liste.Add(temp_Liste.ElementAt(0));
                             temp_Liste.RemoveAt(0);
                         }
                         if (temp_Liste.Count == 0)   
                             unsort = false;
                     }
                     break;
                 case "Postleitzahl":
                     sortierte_Liste.Add(temp_Liste.ElementAt(0));
                     temp_Liste.RemoveAt(0);
                     while (unsort == true)
                     {
                         insert = false;
                         int temp = 0;
                         for (int i = 0; sortierte_Liste.Count > i; i++)
                         {
                             Aufwand++;
                             if (temp_Liste.ElementAt(0).PLZ < sortierte_Liste.ElementAt(temp).PLZ)
                             {
                                 sortierte_Liste.Insert(i, temp_Liste.ElementAt(0));
                                 insert = true;
                                 if (temp_Liste.Count == 0)
                                 {
                                     System.Console.WriteLine("Aufwand= " + Aufwand);
                                     return sortierte_Liste;
                                 }
                             }
                         }
                         if (insert == false)
                         {
                             sortierte_Liste.Add(temp_Liste.ElementAt(0));
                             temp_Liste.RemoveAt(0);
                         }
                         if (temp_Liste.Count == 0)
                             unsort = false;
                     }
                     break;
                 case "Ort":
                     sortierte_Liste.Add(temp_Liste.ElementAt(0));
                     temp_Liste.RemoveAt(0);
                     while (unsort == true)
                     {
                         insert = false;
                         int temp = 0;
                         for (int i = 0; sortierte_Liste.Count > i; i++)
                         {
                             Aufwand++;
                             int sort = temp_Liste.ElementAt(0).Ort.ToLower().CompareTo(sortierte_Liste.ElementAt(i).Ort.ToLower());
                             if (sort == 1)
                             {
                                 sortierte_Liste.Insert(i, temp_Liste.ElementAt(0));
                                 temp_Liste.RemoveAt(0);
                                 insert = true;
                                 if (temp_Liste.Count == 0)
                                 {
                                     System.Console.WriteLine("Aufwand= " + Aufwand);
                                     return sortierte_Liste;
                                 }
                             }
                         }
                         if (insert == false)
                         {
                             sortierte_Liste.Add(Liste.ElementAt(0));
                             temp_Liste.RemoveAt(0);
                         }
                         if (temp_Liste.Count == 0)
                             unsort = false;
                     }
                     break;
                 case "Bettenanzahl":
                     sortierte_Liste.Add(temp_Liste.ElementAt(0));
                     temp_Liste.RemoveAt(0);
                     while (unsort == true)
                     {
                         insert = false;
                         int temp = 0;
                         for (int i = 0; sortierte_Liste.Count > i; i++)
                         {
                             Aufwand++;
                             if (temp_Liste.ElementAt(0).Bettenzahl < sortierte_Liste.ElementAt(temp).Bettenzahl)
                             {
                                 sortierte_Liste.Insert(i, temp_Liste.ElementAt(0));
                                 insert = true;
                                 if (temp_Liste.Count == 0)
                                 {
                                     System.Console.WriteLine("Aufwand= " + Aufwand);
                                     return sortierte_Liste;
                                 }
                             }
                         }
                         if (insert == false)
                             sortierte_Liste.Add(temp_Liste.ElementAt(0));
                         temp_Liste.RemoveAt(0);
                         if (temp_Liste.Count == 0)
                             unsort = false;
                     }
                     break;

             }
             System.Console.WriteLine("Aufwand= " + Aufwand);
             write_data(sortierte_Liste);
             return sortierte_Liste;
         }
//---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------        
        public void search(List<Einrichtung> Liste)
        {             
            System.Console.Write("Gib eine Postleitzahl zwischen 1000 & 99999: ");
            List<Einrichtung> net= new List<Einrichtung>();
            net = erfolglos(Liste);
            
            int Aufwand = 0;
            int Eingabe = Convert.ToInt32(System.Console.Read());
            
                for (int i = 0; i < 1000; i++)
                {
                   // if (Eingabe == net.ElementAt(i).PLZ)
                    if (Eingabe == Liste.ElementAt(i).PLZ)
                    {
                        Aufwand++;
                        System.Console.WriteLine("Postleitzahl gefunden");
                        System.Console.WriteLine(Liste.ElementAt(i).Name + "\t" + Liste.ElementAt(i).Straße + "\t" + Liste.ElementAt(i).PLZ + "\t" + Liste.ElementAt(i).Ort + "\t" + Liste.ElementAt(i).Bettenzahl);
                        System.Console.WriteLine("Aufwand:  " + Aufwand);
                        return;
                    }
                    else { Aufwand++; }
                }
                System.Console.WriteLine("Postleitzahl nicht gefunden.");
                System.Console.WriteLine("Aufwand:  "+Aufwand);
        }

         public void search_binary(List<Einrichtung> Liste)
         {
             List<Einrichtung> Stichprobe=new List<Einrichtung>();
             List<Einrichtung> Teilliste1=new List<Einrichtung>();
             bool gefunden = false;
             int Aufwand = 0;
             int zähler=500;
             for (int i = 0; i < 1000; i++)
             {
                 Stichprobe.Add(Liste.ElementAt(i));
             }
             //List<Einrichtung> Stichprobe = erfolglos(Liste); //für erfolglose suche


            System.Console.Write("Gib eine Postleitzahl zwischen 1000 & 99999: ");
            String SEingabe = System.Console.ReadLine();
            int Eingabe = Convert.ToInt32(SEingabe);        //Eingabe in int umwandeln
         while(gefunden==false|Stichprobe.Count>1){         
             for(int a =0;a<zähler;a++){                    //Stichprobe in 2 hälften aufteilen
             Teilliste1.Add(Stichprobe.ElementAt(0));
             Stichprobe.RemoveAt(0);
             }
             if(Eingabe>=Stichprobe.ElementAt(0).PLZ)       //schau ob in 1 oder 2 hälfte such PLZ ist 
             {
                if(Eingabe==Stichprobe.ElementAt(0).PLZ){   //wenn gleich am anfang der hälfte dann suche beenden und ausgabe
                    System.Console.WriteLine(Stichprobe.ElementAt(0).PLZ);
                    System.Console.WriteLine("Aufwand= " + Aufwand);
                    return;
                }
                Teilliste1.RemoveRange(0, Teilliste1.Count);    //entferne alle einträge in anderen häfte
             }
             else if (Eingabe >= Teilliste1.ElementAt(0).PLZ)  //wenn gleich am anfang der hälfte dann suche beenden und ausgabe
             {
                 if(Eingabe==Stichprobe.ElementAt(0).PLZ){
                    System.Console.WriteLine(Stichprobe.ElementAt(0).PLZ);
                    System.Console.WriteLine("Aufwand= " + Aufwand);
                    return;
                }
                 Stichprobe.RemoveRange(0, Teilliste1.Count); //entferne alle einträge in anderen häfte
                 Stichprobe.AddRange(Teilliste1);             //nutze hälfte mit der such PLZ zur weiteren suche
                 Teilliste1.RemoveRange(0, Teilliste1.Count);
             }
             zähler=zähler/2;                                   //halbiere zähler
             Aufwand++;                                         //erhöhe Aufwand
         }
         System.Console.WriteLine("Aufwand= " + Aufwand);
    }
//-----------------------------------------------------------------------------------------------------------------------------------

         public void search_interpol(List<Einrichtung> Liste)
         {
             List<Einrichtung> Stichprobe = new List<Einrichtung>();

             int Aufwand = 0,links=1, pos = 0;
             float temp=0;
             bool gefunden = false;
             for (int i = 0; i < 1000; i++)     //Stichprobe von 1000 Einträgen in der Liste
                 Stichprobe.Add(Liste.ElementAt(i));

             //List<Einrichtung> Stichprobe = erfolglos(Liste); //für erfolglose Suche
             int rechts=Stichprobe.Count;
             System.Console.Write("Gib eine Postleitzahl zwischen 1000 & 99999: ");
             String SEingabe = System.Console.ReadLine();
             int Eingabe = Convert.ToInt32(SEingabe);       //Postleitzahl in String umwandeln
             while (gefunden == false | Stichprobe.Count > 1)   //solange PLZ nicht gefunden und Stichprobe größer als 1
             {
                 temp = links + ((float)(Eingabe - (Stichprobe.ElementAt(links - 1).PLZ)) / ((float)(Stichprobe.ElementAt(Stichprobe.Count-1).PLZ) - (Stichprobe.ElementAt(links - 1).PLZ))) *Stichprobe.Count - links;
                 //Formel zu berechnen der Position siehe Wikipedia
                 pos = (int)Math.Round(temp);   //auf ganzen wert runden
                 try
                 {
                     if (Stichprobe.ElementAt(pos - 1).PLZ == Eingabe)  //wenn gefunden dann Ausgabe und suche beenden
                     {
                         System.Console.WriteLine(Stichprobe.ElementAt(pos - 1).PLZ);
                         System.Console.WriteLine("Aufwand= " + Aufwand);
                         return;
                     }
                     else if (Stichprobe.ElementAt(pos - 1).PLZ < Eingabe) { /*links = pos + 1;*/ Stichprobe.RemoveRange(0, pos); }
                         //Wenn zu suchende PLZ links dann
                     else if (Stichprobe.ElementAt(pos - 1).PLZ > Eingabe) { /* rechts = pos - 1; */Stichprobe.RemoveRange(pos, Stichprobe.Count - pos); }
                         //Wenn zu suchende PLZ links dann
                        //nur in hälfte in der die zu suchenbde PLZ ist weiter suchen
                     Aufwand++;
                     if (pos == 1000)   //wenn in pos 1000 nicht gefunden dann Abbruch
                     {
                         System.Console.WriteLine("Aufwand= " + Aufwand);
                         return;
                     }
                 }
                 catch (ArgumentOutOfRangeException e) {       
                     System.Console.WriteLine("Aufwand= " + Aufwand);
                     return; }
             }
        }
         private List<Einrichtung> erfolglos(List<Einrichtung> Liste)
         {
             int zähler = 0;
             List<Einrichtung> net =new List<Einrichtung>();
             for (int i = 0; i < 1000; i++)
             {
                 bool nicht_enthalten = false;

                 Einrichtung d = new Einrichtung();
                 while (nicht_enthalten == false)
                 {
                     if (i + 1000 != Liste.ElementAt(zähler).PLZ)
                     {
                         d.PLZ = 1000 + i;
                         nicht_enthalten = true;
                         net.Add(d);
                     }
                     else { zähler++; }
                 }
             }
             return net;
         
         }


        }

   
        class Program
        {
            static void Main(string[] args)
            {
                Daten daten = new Daten();
                List<Einrichtung> Liste = new List<Einrichtung>();
                Liste=daten.read_data();
                daten.write_data(Liste);
               // daten.Ausgabe();
                Liste=daten.Selection_sort();
                daten.search(Liste);
                daten.search_interpol(Liste);
               daten.search_binary(Liste);

               // daten.insertion_sort();
                System.Console.ReadLine();
            }
        }   
}