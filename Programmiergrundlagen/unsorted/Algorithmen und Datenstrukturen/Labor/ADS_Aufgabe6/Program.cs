﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;



namespace ADS_Aufgabe6
{

    class Einrichtung
    {
        public String Name, Straße, Ort;
        public int PLZ, Bettenzahl;
    }

    class Daten
    {
        string Dateiname = @"C:\Users\Thomas\Dropbox\Meins\Studium\Algorithmen & Datenstrukturen\ADS_Aufgabe6\KHR95_red.txt";
        // List<Einrichtung> Liste = new List<Einrichtung>();
        public string Dateiinhalt = "";
        public int Anzahl = 0;

        public List<Einrichtung> read_data()
        {
            List<Einrichtung> Liste = new List<Einrichtung>();
            StreamReader tw = new StreamReader(Dateiname, System.Text.Encoding.Default);
            if (File.Exists(Dateiname))
            {
                Dateiinhalt = tw.ReadLine();    //Kopfzeile überspringen
                Dateiinhalt = tw.ReadLine();     //erste Zeile lesen
                Anzahl++;
                while (Dateiinhalt != null)
                {
                    int index = 0, index2 = 0;
                    Einrichtung einrichtung = new Einrichtung();
                    if (Dateiinhalt != "")
                    {
                        for (int i = 0; i < 5; i++)
                        {

                            switch (i)  //Zeile der Datei durchgehen und Werte rausholen
                            {
                                case 0: einrichtung.Name = Dateiinhalt.Substring(index, index2 = Dateiinhalt.IndexOf("\t", index));
                                    break;
                                case 1:
                                    einrichtung.Straße = Dateiinhalt.Substring(index, index2 = Dateiinhalt.IndexOf("\t", index) - index);
                                    break;
                                case 2: einrichtung.PLZ = Convert.ToInt32(Dateiinhalt.Substring(index, index2 = Dateiinhalt.IndexOf("\t", index) - index));
                                    break;
                                case 3: einrichtung.Ort = Dateiinhalt.Substring(index, index2 = Dateiinhalt.IndexOf("\t", index) - index);
                                    break;
                                case 4: einrichtung.Bettenzahl = Convert.ToInt32(Dateiinhalt.Substring(index, index2 = Dateiinhalt.Length - index));
                                    break;
                            }

                            index = Dateiinhalt.IndexOf("\t", index);   //Zum nächsten <TAB> gehen und index holen
                            index++;                                    //Index um 1 erhöhen damit er nicht auf den <TAB> ist
                        }
                        Liste.Add(einrichtung);                         //EInrichtung zur Liste hinzufügen
                        Dateiinhalt = tw.ReadLine();                    //nächste Zeile lesen
                        Anzahl++;
                    }
                    Dateiinhalt = tw.ReadLine();                    //nächste Zeile lesen
                        
                }
            }
            tw.Close();
            return Liste;                                          //Liste mit Einrichtungen zurückgeben
        }
        public void write_data(List<Einrichtung> Liste)
        {
            {
                TextWriter tw = new StreamWriter(@"C:\Users\Thomas\Dropbox\Meins\Studium\Algorithmen & Datenstrukturen\ADS_Aufgabe6\KHR95.txt", false, Encoding.Default); //false damit er nicht am ende Anhängt sonder 
                //Encoding default damit er Sonderzeichen schreibt

                tw.WriteLine("Name" + "\t" + "StrHausNr" + "\t" + "PLZ" + "\t" + "Ort" + "\t" + "Bettenanzahl");             // in erste Zeile Tabellenheader schreiben
                for (int i = 0; Liste.Count > i; i++)                                       // Liste in Datei schreiben
                {
                    Einrichtung einrichtung = Liste.ElementAt(i);
                    tw.WriteLine(einrichtung.Name + "\t" + einrichtung.Straße + "\t" + einrichtung.PLZ + "\t" + einrichtung.Ort + "\t" + einrichtung.Bettenzahl);
                }
                tw.Close();
            }
        }

        public void Ausgabe()
        {
            int Eingabe = 0;
            StreamReader tw = new StreamReader(Dateiname, System.Text.Encoding.Default);

            System.Console.WriteLine("Es gibt " + Anzahl + " Records.");
            System.Console.Write("Gib ein Record an das ausgegeben werden soll: ");
            Eingabe = Convert.ToInt32(System.Console.ReadLine());
            for (int i = 0; i < Eingabe; i++)
            {
                if (File.Exists(Dateiname))
                {
                    tw.ReadLine();
                }
            }
            System.Console.WriteLine(tw.ReadLine());
        }

        //------------------------------------------------Ab hier Aufgabe 7 mit den Sortierungen----------------------------------------------------------------;
        public List<Einrichtung> Selection_sort(List<Einrichtung> Liste,String Sortierung)
        {
           // List<Einrichtung> Liste = read_data();       //Liste holen
            List<Einrichtung> sortierte_Liste = new List<Einrichtung>();
            List<Einrichtung> temp_Liste = new List<Einrichtung>();
            temp_Liste.AddRange(Liste);
            int zähler = 0;

            bool unsort = true;
            //String Sortierung;

            //System.Console.Write("Nach was soll sortiert werden: ");
            //Sortierung = System.Console.ReadLine();
            int vergleich = 0;
            int Aufwand = 0;
            switch (Sortierung)   //nach was soll sortiert werden 
            {                     //nach Aufgabenstellung sind nicht alle Sortierungen notwendig
                case "Name":
                    while (unsort == true)    //solange nicht gesamte Datei sortiert wurde
                    {
                        int temp = 0;
                        Sortierung = temp_Liste.ElementAt(0).Name.ToLower();    //nim ersten Namen in der Liste
                        for (int i = 1; temp_Liste.Count > i; i++)              //geh jeden Namen in der unsortierten Liste durch
                        {
                            Aufwand++;                                          //erhöhr den Aufwand bei jedem durchlauf um 1
                            int sort = Sortierung.CompareTo(temp_Liste.ElementAt(i).Name.ToLower());    //Vergleiche
                            if (sort == 1)     //0=sind gleich; 1= ist kleiner; -1= ist größer
                            {
                                temp = i;
                                Sortierung = temp_Liste.ElementAt(i).Name.ToLower();    //weiter mit dem Vergleichen
                            }
                        }
                        sortierte_Liste.Add(Liste.ElementAt(temp));     //Einrichtung in sortierte Liste eintragen
                        temp_Liste.RemoveAt(temp);                      //eingetragene Einrichtung in unsortierte Liste löschen
                        if (temp_Liste.Count == 0)                      //wenn unsortierte Liste leer Sortiervorgang fertig
                            unsort = false;
                    }
                    break;      //alle anderen laufen gleich ab unterschied nur nach was sortiert wird

                case "Straße":
                    while (unsort == true)
                    {
                        int temp = 0;
                        Sortierung = temp_Liste.ElementAt(0).Straße.ToLower();
                        for (int i = 1; Liste.Count > i; i++)
                        {
                            Aufwand++;
                            int sort = Sortierung.CompareTo(temp_Liste.ElementAt(i).Straße.ToLower());
                            if (sort == 1)
                            {
                                temp = i;
                                Sortierung = temp_Liste.ElementAt(i).Straße.ToLower();
                            }
                        }
                        sortierte_Liste.Add(temp_Liste.ElementAt(temp));
                        temp_Liste.RemoveAt(temp);
                        if (temp_Liste.Count == 0)
                            unsort = false;
                    }
                    break;

                case "Postleitzahl":
                    while (unsort == true)
                    {
                        int temp = 0;

                        for (int i = 1; temp_Liste.Count > i; i++)
                        {
                            Aufwand++;
                            if (temp_Liste.ElementAt(i).PLZ < temp_Liste.ElementAt(temp).PLZ)   //Vergleich
                            {
                                temp = i;
                                vergleich = temp_Liste.ElementAt(i).PLZ;
                            }
                        }
                        sortierte_Liste.Add(temp_Liste.ElementAt(temp));
                        temp_Liste.RemoveAt(temp);
                        if (temp_Liste.Count == 0)
                            unsort = false;
                    }
                    break;

                case "Ort":
                    while (unsort == true)
                    {
                        int temp = 0;
                        Sortierung = temp_Liste.ElementAt(0).Ort.ToLower();
                        for (int i = 1; temp_Liste.Count > i; i++)
                        {
                            Aufwand++;
                            int sort = Sortierung.CompareTo(temp_Liste.ElementAt(i).Ort.ToLower());
                            if (sort == 1)
                            {
                                temp = i;
                                Sortierung = temp_Liste.ElementAt(i).Ort.ToLower();
                            }
                        }
                        sortierte_Liste.Add(temp_Liste.ElementAt(temp));
                        temp_Liste.RemoveAt(temp);
                        if (temp_Liste.Count == 0)
                            unsort = false;
                    }
                    break;

                case "Bettenanzahl":
                    while (unsort == true)
                    {
                        int temp = 0;
                        for (int i = 1; temp_Liste.Count > i; i++)
                        {
                            Aufwand++;
                            if (temp_Liste.ElementAt(i).Bettenzahl < temp_Liste.ElementAt(temp).Bettenzahl)
                            {
                                temp = i;
                                vergleich = temp_Liste.ElementAt(i).Bettenzahl;
                            }
                        }
                        sortierte_Liste.Add(temp_Liste.ElementAt(temp));
                        temp_Liste.RemoveAt(temp);
                        if (temp_Liste.Count == 0)
                            unsort = false;
                    }
                    break;
            }
           // System.Console.WriteLine("Aufwand= " + Aufwand);
            write_data(sortierte_Liste);
            return sortierte_Liste;
        }


        //----------------------------------------------------------------------------------------------------------------------------------------------

        public List<Einrichtung> insertion_sort(List<Einrichtung> Liste)
        {
           // List<Einrichtung> Liste = read_data();
            List<Einrichtung> temp_Liste = new List<Einrichtung>();
            temp_Liste.AddRange(Liste);
            List<Einrichtung> sortierte_Liste = new List<Einrichtung>();
            bool unsort = true;
            bool insert = false;
            String Sortierung;

            System.Console.Write("Nach was soll sortiert werden: ");
            Sortierung = System.Console.ReadLine();
            int vergleich = 0;
            int Aufwand = 0;
            int tausch = 0;
            switch (Sortierung)        //nach was soll sortiert werden
            {
                case "Name":
                    sortierte_Liste.Add(Liste.ElementAt(0));   //ersten eintrag in sortierte Liste eintragen
                    temp_Liste.RemoveAt(0);                    //Eintrag aus unsortierte löschen
                    while (unsort == true)                     //solange nicht vollständigt sortiert wurde
                    {
                        insert = false;
                        int temp = 0;
                        if (temp_Liste.Count != 0)
                            for (int i = 0; sortierte_Liste.Count > i; i++)    //alle Einträge in sortierte Liste durchgehen durchgehen
                            {
                                Aufwand++;
                                int sort = temp_Liste.ElementAt(0).Name.ToLower().CompareTo(sortierte_Liste.ElementAt(i).Name.ToLower());
                                //vergleich Eintrag aus unsortierter Liste mit allen aus sortierter Liste


                                if (sort == -1)     //füge in sortierte Liste Eintrag ein an der Stelle 
                                {
                                    sortierte_Liste.Insert(i, temp_Liste.ElementAt(0));
                                    temp_Liste.RemoveAt(0);
                                    insert = true;
                                    if (temp_Liste.Count == 0)     //wenn sortierug fertig Aufwands Ausgabe und Rückgabe der sortierten Liste
                                    {
                                        System.Console.WriteLine("Aufwand= " + Aufwand);
                                        return sortierte_Liste;
                                    }
                                }
                            }
                        if (insert == false && temp_Liste.Count != 0)        //wenn nichts größeres gefunden wurde dann füge am ende an
                        {
                            sortierte_Liste.Add(Liste.ElementAt(0));
                            temp_Liste.RemoveAt(0);
                        }
                        if (temp_Liste.Count == 0)
                            unsort = false;
                    }
                    break;     //Ablauf bei allen anderen gleich

                case "Straße":
                    sortierte_Liste.Add(Liste.ElementAt(0));
                    temp_Liste.RemoveAt(0);
                    while (unsort == true)
                    {
                        insert = false;
                        int temp = 0;
                        for (int i = 0; sortierte_Liste.Count > i; i++)
                        {
                            Aufwand++;
                            int sort = temp_Liste.ElementAt(0).Straße.ToLower().CompareTo(sortierte_Liste.ElementAt(i).Straße.ToLower());
                            if (sort == -1)
                            {
                                sortierte_Liste.Insert(i, temp_Liste.ElementAt(0));
                                temp_Liste.RemoveAt(0);
                                insert = true;
                                if (temp_Liste.Count == 0)
                                {
                                    System.Console.WriteLine("Aufwand= " + Aufwand);
                                    return sortierte_Liste;
                                }
                            }
                        }
                        if (insert == false)
                        {
                            sortierte_Liste.Add(temp_Liste.ElementAt(0));
                            temp_Liste.RemoveAt(0);
                        }
                        if (temp_Liste.Count == 0)
                            unsort = false;
                    }
                    break;
                case "Postleitzahl":
                    sortierte_Liste.Add(temp_Liste.ElementAt(0));
                    temp_Liste.RemoveAt(0);
                    while (unsort == true)
                    {
                        insert = false;
                        int temp = 0;
                        for (int i = 0; sortierte_Liste.Count > i; i++)
                        {
                            Aufwand++;
                            if (temp_Liste.ElementAt(0).PLZ < sortierte_Liste.ElementAt(temp).PLZ)
                            {
                                sortierte_Liste.Insert(i, temp_Liste.ElementAt(0));
                                insert = true;
                                if (temp_Liste.Count == 0)
                                {
                                    System.Console.WriteLine("Aufwand= " + Aufwand);
                                    return sortierte_Liste;
                                }
                            }
                        }
                        if (insert == false)
                        {
                            sortierte_Liste.Add(temp_Liste.ElementAt(0));
                            temp_Liste.RemoveAt(0);
                        }
                        if (temp_Liste.Count == 0)
                            unsort = false;
                    }
                    break;
                case "Ort":
                    sortierte_Liste.Add(temp_Liste.ElementAt(0));
                    temp_Liste.RemoveAt(0);
                    while (unsort == true)
                    {
                        insert = false;
                        int temp = 0;
                        for (int i = 0; sortierte_Liste.Count > i; i++)
                        {
                            Aufwand++;
                            int sort = temp_Liste.ElementAt(0).Ort.ToLower().CompareTo(sortierte_Liste.ElementAt(i).Ort.ToLower());
                            if (sort == 1)
                            {
                                sortierte_Liste.Insert(i, temp_Liste.ElementAt(0));
                                temp_Liste.RemoveAt(0);
                                insert = true;
                                if (temp_Liste.Count == 0)
                                {
                                    System.Console.WriteLine("Aufwand= " + Aufwand);
                                    return sortierte_Liste;
                                }
                            }
                        }
                        if (insert == false)
                        {
                            sortierte_Liste.Add(Liste.ElementAt(0));
                            temp_Liste.RemoveAt(0);
                        }
                        if (temp_Liste.Count == 0)
                            unsort = false;
                    }
                    break;
                case "Bettenanzahl":
                    sortierte_Liste.Add(temp_Liste.ElementAt(0));
                    temp_Liste.RemoveAt(0);
                    while (unsort == true)
                    {
                        insert = false;
                        int temp = 0;
                        for (int i = 0; sortierte_Liste.Count > i; i++)
                        {
                            Aufwand++;
                            if (temp_Liste.ElementAt(0).Bettenzahl < sortierte_Liste.ElementAt(temp).Bettenzahl)
                            {
                                sortierte_Liste.Insert(i, temp_Liste.ElementAt(0));
                                insert = true;
                                if (temp_Liste.Count == 0)
                                {
                                    System.Console.WriteLine("Aufwand= " + Aufwand);
                                    return sortierte_Liste;
                                }
                            }
                        }
                        if (insert == false)
                            sortierte_Liste.Add(temp_Liste.ElementAt(0));
                        temp_Liste.RemoveAt(0);
                        if (temp_Liste.Count == 0)
                            unsort = false;
                    }
                    break;

            }
            //System.Console.WriteLine("Aufwand= " + Aufwand);
            write_data(sortierte_Liste);
            return sortierte_Liste;
        }
        //---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------        
        public void search(List<Einrichtung> Liste)
        {
            System.Console.Write("Gib eine Postleitzahl zwischen 1000 & 99999: ");
            List<Einrichtung> net = new List<Einrichtung>();
            net = erfolglos(Liste);
            int zähler = 0;
            int Aufwand = 0;
            //int Eingabe = Convert.ToInt32(System.Console.ReadLine());
            while(zähler<1000){
            for (int i = 0; i < Liste.Count-1; i++)
            {
                // if (Eingabe == net.ElementAt(i).PLZ)
                if (net.ElementAt(zähler).PLZ == Liste.ElementAt(i).PLZ)
                {
                    Aufwand++;
                    System.Console.WriteLine("Postleitzahl gefunden");
                    System.Console.WriteLine(Liste.ElementAt(i).Name + "\t" + Liste.ElementAt(i).Straße + "\t" + Liste.ElementAt(i).PLZ + "\t" + Liste.ElementAt(i).Ort + "\t" + Liste.ElementAt(i).Bettenzahl);
                    System.Console.WriteLine("Aufwand:  " + Aufwand);
                    return;
                }
                else { Aufwand++; }
            }
            zähler++;
            }
            System.Console.WriteLine("Postleitzahl nicht gefunden.");
            System.Console.WriteLine("Aufwand:  " + Aufwand);
        }
//----------------------------------------------------------------------------------------------------------------------------
        public void search_binary(List<Einrichtung> Liste)
        {
            //List<Einrichtung> Stichprobe = new List<Einrichtung>();
            List<Einrichtung> Teilliste1 = new List<Einrichtung>();
            List<Einrichtung> Stichprobe=new List<Einrichtung>();
            bool gefunden = false;
            int Aufwand = 0;
            int zähler = Liste.Count / 2;
            int temp=0;
            String Eingabe;
            System.Console.WriteLine("Eingabe");
            Eingabe = System.Console.ReadLine();
            if (Eingabe.Contains("el"))
            {
                 Stichprobe = erfolglos(Liste);
            }//für erfolglose Suche
            else if (Eingabe.Contains("er"))
            {
                 Stichprobe = erfolgreich(Liste);
            }//für erfolglose SucheList<Einrichtung> tem_Liste= new List<Einrichtung>();
            List<Einrichtung> tem_Liste=new List<Einrichtung>();
            tem_Liste.AddRange(Liste);
            while (temp < 1000)
            {
                while (gefunden == false & Liste.Count > 1)
                {
                    for (int a = 0; a < zähler; a++)
                    {                    //Stichprobe in 2 hälften aufteilen
                        Teilliste1.Add(Liste.ElementAt(0));
                        Liste.RemoveAt(0);
                    }
                    if (Stichprobe.ElementAt(temp).PLZ >= Liste.ElementAt(0).PLZ)       //schau ob in 1 oder 2 hälfte such PLZ ist 
                    {
                        if (Stichprobe.ElementAt(temp).PLZ == Liste.ElementAt(0).PLZ)
                        {   //wenn gleich am anfang der hälfte dann suche beenden und ausgabe
                           // System.Console.WriteLine(Liste.ElementAt(0).PLZ);
                           // System.Console.WriteLine("Aufwand= " + Aufwand);
                            break;
                        }
                        Teilliste1.RemoveRange(0, Teilliste1.Count);    //entferne alle einträge in anderen häfte
                    }
                    else if(Teilliste1.Count>0){

                        if (Stichprobe.ElementAt(temp).PLZ >= Teilliste1.ElementAt(0).PLZ)  //wenn gleich am anfang der hälfte dann suche beenden und ausgabe
                    
                        if (Stichprobe.ElementAt(temp).PLZ == Liste.ElementAt(0).PLZ)
                        {
                            //System.Console.WriteLine(Liste.ElementAt(0).PLZ);
                            //System.Console.WriteLine("Aufwand= " + Aufwand);
                            break;
                        }
                        Liste.RemoveRange(0, Teilliste1.Count); //entferne alle einträge in anderen häfte
                        Liste.AddRange(Teilliste1);             //nutze hälfte mit der such PLZ zur weiteren suche
                        Teilliste1.RemoveRange(0, Teilliste1.Count);
                    }
                    zähler = Liste.Count / 2;                                   //halbiere zähler
                    Aufwand++;                                         //erhöhe Aufwand
                }
                Liste.RemoveAt(0);
                Liste.AddRange(tem_Liste);
                temp++;
            }
            System.Console.WriteLine("Aufwand= " + Aufwand);
        }
        //-----------------------------------------------------------------------------------------------------------------------------------

        public void search_interpol(List<Einrichtung> Liste)
        {
            String Eingabe;
            List<Einrichtung> Stichprobe = new List<Einrichtung>();
            System.Console.WriteLine("Eingabe");
            Eingabe=System.Console.ReadLine();
            if(Eingabe=="el"){
            Stichprobe = erfolglos(Liste); }//für erfolglose Suche
            else if (Eingabe == "er")
            {
               Stichprobe = erfolgreich(Liste);
            }//für erfolglose Suche
            List<Einrichtung> tem_Liste = new List<Einrichtung>();
            int Aufwand = 0;
            int temp2 = 0;
            while (temp2 < 1000)
            {
                int links =1;                  // linke Teilfeldbegrenzung
                int rechts = Liste.Count-1;  // rechte Teilfeldbegrenzung
                int versch=0;                     // Anzahl verschiedener Elemente
                int pos=0;                        // aktuelle Teilungsposition

                // solange der Schlüssel im Bereich liegt (andernfalls ist das gesuchte
                // Element nicht vorhanden)
                while (Stichprobe.ElementAt(temp2).PLZ >= Liste.ElementAt(links).PLZ && Stichprobe.ElementAt(temp2).PLZ <= Liste.ElementAt(rechts).PLZ)
                {
                    Aufwand++;
                    
                    // Aktualisierung der Anzahl der verschiedenen Elemente
                    versch = Liste.ElementAt(rechts).PLZ - Liste.ElementAt(links-1).PLZ;

                    // Berechnung der neuen interpolierten Teilungsposition
                    pos = links + (int)(((double)versch) * (rechts+1 - links)
                        / versch);

                    try
                    {
                        
                        if (Stichprobe.ElementAt(temp2).PLZ > Liste.ElementAt(pos).PLZ)            // rechtes Teilintervall
                            links = pos + 1;
                        else if (Stichprobe.ElementAt(temp2).PLZ < Liste.ElementAt(pos).PLZ)      // linkes Teilintervall
                            rechts = pos - 1;
                        else if (Stichprobe.ElementAt(temp2).PLZ == Liste.ElementAt(pos).PLZ)
                        {
                            //System.Console.WriteLine(Stichprobe.ElementAt(temp2).PLZ+"    "+ Liste.ElementAt(pos).PLZ);
                            break;
                        }
                        
                        // Element gefunden
                        //break;                             // Position zurückgeben
                    }
                    catch (Exception e) {
                        //Aufwand++;
                        Aufwand++;
                        break; }
                    
                }
                // break; -1;                                 // Element nicht gefunden
                temp2++;
            }
            System.Console.WriteLine("Aufwand: "+Aufwand);
        }

        public List<Einrichtung> erfolglos(List<Einrichtung> Liste)
        {
            int PLZ = 0;
            Random rnd = new Random();
            List<Einrichtung> net = new List<Einrichtung>();
            while (net.Count < 1000)
            {
                PLZ = rnd.Next(1000, 99999);           //Zufallszahl zw. 1000-99999
                int zähler = 0;
                bool enthalten = false;

                Einrichtung d = new Einrichtung();
                while (enthalten == false)             //prüfen ob Zufalllszahl in scon als Postleitzahl existiert
                {
                    if (PLZ == Liste.ElementAt(zähler).PLZ)    //wenn ja verlasse Schleife
                    {
                        enthalten = true;
                    }
                    else if (zähler == Liste.Count - 1)        //wenn nicht dann füge an
                    {
                        enthalten = true;
                        d.PLZ = PLZ;
                        net.Add(d);
                    }
                    zähler++;
                }

            }
            return net;
        }

        public List<Einrichtung> erfolgreich(List<Einrichtung> Liste)      //1000 zufällige datensätze aus der Liste holen
        {
            Random rnd = new Random();
            int PLZ = 0;
            List<Einrichtung> net = new List<Einrichtung>();
            while (net.Count < 1000)
            {
                PLZ = rnd.Next(0, Liste.Count - 1);
                net.Add(Liste.ElementAt(PLZ));
            }
            
            return net;
        }
    }
   
        class Program
        {
            static void Main(string[] args)
            {
                Daten daten = new Daten();
                List<Einrichtung> Liste = new List<Einrichtung>();
                List<Einrichtung> temp = new List<Einrichtung>();
                Liste=daten.read_data();
                daten.write_data(Liste);
               // daten.Ausgabe();
                Liste=daten.Selection_sort(Liste,"Postleitzahl");
                temp = daten.erfolglos(Liste);
                temp = daten.Selection_sort(temp, "Postleitzahl");
              //  daten.search(Liste);
                daten.search_binary(Liste);
                daten.search_interpol(Liste);

               // daten.insertion_sort();
                System.Console.ReadLine();
            }
        }   
}