﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ADS_Aufgabe4
{
    class Liste
    {
        Knoten element;
        StringBuilder trav;
        bool s1 = true;
        int leftsize = 1;
        public void insert_right()       //rechts vom ausgehenden Knoten einnen Knoten einsetzen
        {
            if (element == null)
            {
                element = new Knoten();
            }
            else
            {
                Knoten neu = new Knoten();
                neu.previous = element;
                element.right = neu;
            }
        }

        public void insert_left()       //links vom ausgehenden Knoten einnen Knoten einsetzen
        {
            leftsize++;
            if (element == null)
            {
                element = new Knoten();
            }
            else
            {
                Knoten neu = new Knoten();
                neu.previous = element;
                element.left = neu;
            }
        }

        public String insert_String(String _Inhalt) //String einsetzen
        {
            char[] Inarr;
            int i = 0;
            Inarr = _Inhalt.ToCharArray();
            element = new Knoten();
            while (Inarr.Length != i)
            {
                if ((Inarr[i] == '.') | (Inarr[i] == '_') | (Inarr[i] == ' ') | (Inarr[i] == ',') | (Inarr[i] == ';') | (Inarr[i] == '!') | (Inarr[i] == '?'))  // bei Leer, Unterstrich oder Satzzeichen nach links vom Stammknoten 
                {
                    while (element.previous != null)    //zurück zur Wurzel
                    {
                        element = element.previous;
                    }
                    while (element.left != null)        //nach ganz links
                    {
                        element = element.left;
                    }
                    insert_left();
                    element = element.left;
                    element.Inhalt = Inarr[i];
                    i++;
                }
                else
                {
                    while (element.left != null)        //nach ganz links
                    {
                        element = element.left;
                    }
                    while (element.right != null)       //nach ganz rechts
                    {
                        element = element.right;

                    }
                    if (element.Inhalt == '\0')         //wenn Knoten leer schreib was rein
                    {
                        element.Inhalt = Inarr[i];
                        i++;
                    }
                    else
                    {

                        insert_right();
                        element = element.right;
                        element.Inhalt = Inarr[i];
                        i++;
                    }
                }
            }


            return _Inhalt;
        }

        public StringBuilder preorder()
        {
            int rightzaeler=0;
            trav = new StringBuilder();
            while (element.previous != null) { element = element.previous; }    //zur Wurzel
            trav.Append(element.Inhalt);
            while (element.left != null)                                        //nach links & jeden Knoteninhalt speichern
            {
                element = element.left;
                trav.Append(element.Inhalt);
            }
            for(int leftzaehler = 1;leftzaehler<=leftsize;leftzaehler++)        
            {
                if (element.right != null)
                {
                    while (element.right != null)                                   //nach rechts & jeden Knoteninhalt speichern
                    {
                        element = element.right;
                        trav.Append(element.Inhalt);
                        rightzaeler++;
                    }
                    rightzaeler = back_from_right(rightzaeler);
                }
                if(element.previous!= null)
                element = element.previous;
            }
            return trav;
        }

        public StringBuilder inorder()
        {
            int rightzaeler = 0;
            trav = new StringBuilder();
            while (element.left != null) { element = element.left; }    //nach ganz links
           
            for (int leftzaehler = 1; leftzaehler <= leftsize; leftzaehler++)   //zurück zur Wurzel
            {
                trav.Append(element.Inhalt);
                if (element.right != null)
                {
                    while (element.right != null)                            //nach rechts & bei jeden Knoteninhalt speichern
                    {
                        element = element.right;
                        rightzaeler++;
                        trav.Append(element.Inhalt);
                    }
                    rightzaeler = back_from_right(rightzaeler);             //zurück aus rechten Baum
                }
                if (element.previous != null)
                    element = element.previous;
            }
            return trav;
        }

        public StringBuilder postorder()
        {
            int rightzaeler = 1;
            bool zurueck = false;
            trav = new StringBuilder();
            while (element.left != null) { element = element.left; }    //nach ganz links
            for (int leftzaehler = 1; leftzaehler != leftsize; leftzaehler++)
            {
                if (element.right != null)
                {
                    while (element.right != null)                            //nach rechts & jeden Knoteninhalt speichern
                    {
                        element = element.right;
                        rightzaeler++;
                    }
                    while (element.right == null | zurueck == true) //zurück zum Anfang des Knoten & jeden Knoteninhalt speichern
                    {
                        trav.Append(element.Inhalt);
                        zurueck = true;
                        element = element.previous;
                        rightzaeler--;
                        if (rightzaeler == 0)
                            zurueck = false;
                    }
                }

                trav.Append(element.Inhalt);
                if (element.previous != null)
                    element = element.previous;
            }
            return trav;
        }

        public StringBuilder levelorder()       //Level order ist falsch muss korrigiert werden
        {
            int rightzaeler = 0;
            trav = new StringBuilder();
            while (element.previous != null) { element = element.previous; }    //zur Wurzel
            trav.Append(element.Inhalt);
            while (element.left != null)            //linken dann rechten Knoten Besuchen & jeden Knoteninhalt speichern
            {
                element = element.left;
                trav.Append(element.Inhalt);
                element = element.previous;
                if (element.right != null)
                {
                    element = element.right;
                    trav.Append(element.Inhalt);
                    element = element.previous;
                }
                element = element.left;
            }
            for (int leftzaehler = 1; leftzaehler <= leftsize; leftzaehler++)   //nach rechts den ersten überspringen & jeden Knoteninhalt speichern
            {
                if (element.right != null)
                {
                    element = element.right;
                    trav.Append(element.Inhalt);
                    rightzaeler++;
                }
                else 
                {
                    element = element.previous;
                    element = element.right;
                    rightzaeler++;
                }
                if (element.right != null)
                {
                    while (element.right != null)
                    {
                        element = element.right;
                        rightzaeler++;
                        trav.Append(element.Inhalt);
                    }

                    rightzaeler = back_from_right(rightzaeler);
                }
                if (element.previous != null)
                {
                    element = element.previous;
                    element = element.right;
                    rightzaeler++;
                }
            }
            return trav;
        }

        public int back_from_right(int rightzaeler)
        {
            bool zurueck = false;
            while (element.right == null | zurueck == true) //zurück zum Anfang des Knoten
            {
                zurueck = true;
                element = element.previous;
                rightzaeler--;
                if (rightzaeler <= 0)
                    zurueck = false;
            }
            return rightzaeler;
        }

        public int[] Kennzahlen()
        {
            int Knotenzahl = 0, Blätter = 0, maxPfad = 0, Höhe = 0, rightzaeler = 0, i = 4;
            int[] Pfadlänge =new int[leftsize];
            while (element.previous != null) { element = element.previous; }    //zur Wurzel
            for (int leftzaehler = 1; leftzaehler <= leftsize; leftzaehler++)   //nach ganz links
            {
                Pfadlänge[leftzaehler - 1]++;   //Pfadlänge für den jeweiliegen rechten Teilbaum +1
                while (element.right != null)  //zum ende des rechten Teilbaum
                {
                    Knotenzahl++; rightzaeler++; Pfadlänge[leftzaehler - 1]++; //Pfadlänge für den jeweiliegen Rechtenteilbaum +1
                    element = element.right;
                }
                Blätter++;
                rightzaeler = back_from_right(rightzaeler); //zurück aus dem linken Teilbaum
                element = element.left;                     
                Knotenzahl++;
                if (maxPfad < Pfadlänge[leftzaehler - 1])   //bestimmen des maximalen Pfades
                { 
                    maxPfad = Pfadlänge[leftzaehler - 1];
                    Höhe = leftzaehler  + maxPfad;
                }
            }
            int[] Kennzahlen = new int[4+Pfadlänge.Length];
            Kennzahlen[0] = Knotenzahl;
            Kennzahlen[1] = Blätter;
            Kennzahlen[2] = maxPfad;
            Kennzahlen[3] = Höhe;
            while (i != (Pfadlänge.Length + 4))
            {
                Kennzahlen[i] = Pfadlänge[i-4];
                i++;
            }
            return Kennzahlen;
        }
    }

    class Knoten
    {
        public Knoten left, right, previous;
        public char Inhalt, temp;

        public Knoten()
        {
            left = null;
            right = null;
            previous = null;

        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Liste t = new Liste();
            int[] zahlen;
            int i = 4;
            t.insert_String("Dies_ist_ein_binaerer_Baum_zum_traversieren.");
            System.Console.WriteLine("Traversierung");
            System.Console.WriteLine("Orginal Text: Dies_ist_ein_binaerer_Baum_zum_traversieren.");
            System.Console.WriteLine("Pre-order: "+t.preorder());
            System.Console.WriteLine("In-order: " + t.inorder());
            System.Console.WriteLine("Post-order: " + t.postorder());
            System.Console.WriteLine("Level-order: " + t.levelorder());      //Level order ist falsch muss korrigiert werden
            System.Console.WriteLine();
            zahlen=t.Kennzahlen();
            System.Console.WriteLine("Knotenzahl: " + zahlen[0]);
            System.Console.WriteLine("Blätter: " + zahlen[1]);
            System.Console.WriteLine("maimale Pfadlänge: " + zahlen[2]);
            System.Console.WriteLine("Höhe: " + zahlen[3]);

            while (i != zahlen.Length)
            {
                System.Console.WriteLine("Pfadlänge{0}: "  + zahlen[i],i-3);
                i++;
            }
            System.Console.Read();
        }
    }
}
