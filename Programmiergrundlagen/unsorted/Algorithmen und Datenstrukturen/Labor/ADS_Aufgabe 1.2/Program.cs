﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace ADS_Aufgabe_1._2
{

    public class Datum      //Datumsklasse 
    {
        private String Geburtsdatum;
        public String _Geburtsdatum
        {
            get { return Geburtsdatum; }
            set { Geburtsdatum = value; }
        }
    }

    public class Record
    {
        int index = 0;
        Stammdaten[] element;

        public void insert_element()          //hinzufügen eines elementes  
        {
            Datum Geburtsdatum = new Datum();   
                Stammdaten daten = new Stammdaten();
                if (element == null)
                {
                    element = new Stammdaten[10];
                    System.Console.Write("Vorname: ");
                    daten.Vorname = System.Console.ReadLine();
                    System.Console.Write("Nachname: ");
                    daten.Nachname = System.Console.ReadLine();
                    System.Console.Write("Geburtstag: ");
                    Geburtsdatum._Geburtsdatum = System.Console.ReadLine();
                    daten.Geburtsdatum = Geburtsdatum;
                    System.Console.Write("Versicherungsnummer: ");
                    daten.Versicherungsnummer = System.Convert.ToInt32(System.Console.ReadLine());
                    daten.previous = -1;
                    daten.next = -1;
                    element[index] = daten;
                    index++;
                }
                else {
                    System.Console.Write("Vorname: ");
                    daten.Vorname = System.Console.ReadLine();
                    System.Console.Write("Nachname: ");
                    daten.Nachname = System.Console.ReadLine();
                    System.Console.Write("Geburtstag: ");
                    Geburtsdatum._Geburtsdatum = System.Console.ReadLine();
                    daten.Geburtsdatum = Geburtsdatum;
                    System.Console.Write("Versicherungsnummer: ");
                    daten.Versicherungsnummer = System.Convert.ToInt32(System.Console.ReadLine());
                    daten.previous = index-1;
                    daten.next = -1;
                    element[index] = daten;
                    element[index - 1].next = index;
                    index++;
                
                }
        }

        public void print_element()     //Ausgab eines Elements aus dem Record
        {

            System.Console.Write("Welches Element soll ausgegeben werden: ");
            int Eingabe = Convert.ToInt32(System.Console.ReadLine());
            try
            {
                System.Console.WriteLine(element[Eingabe - 1].Vorname);
                System.Console.WriteLine(element[Eingabe - 1].Nachname);
                System.Console.WriteLine(element[Eingabe - 1].Geburtsdatum._Geburtsdatum);
                System.Console.WriteLine(element[Eingabe - 1].Versicherungsnummer);
            }
            catch (Exception e)
            { System.Console.Write("Element existiert nicht."); }
        }

        public void count_element()     //Anzahl der gefüllten elemente zählen
        {
            int index = 0, Zähler = 0, indexrecord = 0;
            while (element.Length > Zähler)
            {
                if (element[Zähler] != null)
                    index++;
                Zähler++;
            }
            System.Console.WriteLine("Record hat {0} gefüllte Elemente.", index);
        }

        public void find_element()      //element suchen
        {
            int zaeler = 0, VN;
            bool gefunden = false;
            System.Console.Write("Was soll gesucht werden: ");
            String suche = System.Console.ReadLine();
            System.Console.Write("Was sind die Daten die gesucht werden sollen: ");
            // try{
            String Daten = System.Console.ReadLine();
            //}
            //catch (Exception e) { VN = Convert.ToInt32(System.Console.ReadLine()); }
            switch (suche)      //nach was soll gesucht werden
            {
                case "Name":
                    while ((gefunden == false & zaeler < element.Length) & element[zaeler] != null)     //solange noch nicht gefunden und zähler ans ende des record gelangt ist
                    {
                        if (element[zaeler].Nachname == Daten)  //wenn gefunden ausgabe
                        {
                            System.Console.WriteLine(element[zaeler].Vorname);
                            System.Console.WriteLine(element[zaeler].Nachname);
                            System.Console.WriteLine(element[zaeler].Geburtsdatum._Geburtsdatum);
                            System.Console.WriteLine(element[zaeler].Versicherungsnummer);
                            gefunden = true;
                        }
                        zaeler++;
                    }
                    break;
                case "Vorname":
                    while ((gefunden == false & zaeler < element.Length) & element[zaeler] != null)
                    {
                        if (element[zaeler].Vorname == Daten)
                        {
                            System.Console.WriteLine(element[zaeler].Vorname);
                            System.Console.WriteLine(element[zaeler].Nachname);
                            System.Console.WriteLine(element[zaeler].Geburtsdatum._Geburtsdatum);
                            System.Console.WriteLine(element[zaeler].Versicherungsnummer);
                            gefunden = true;
                        }
                        zaeler++;
                    }
                    break;

                case "Nachname":
                    while ((gefunden == false & zaeler < element.Length) & element[zaeler] != null)
                    {
                        if (element[zaeler].Nachname == Daten)
                        {
                            System.Console.WriteLine(element[zaeler].Vorname);
                            System.Console.WriteLine(element[zaeler].Nachname);
                            System.Console.WriteLine(element[zaeler].Geburtsdatum._Geburtsdatum);
                            System.Console.WriteLine(element[zaeler].Versicherungsnummer);
                            gefunden = true;
                        }
                        zaeler++;
                    }
                    break;

                case "Geburtsdatum":
                    while ((gefunden == false | zaeler < element.Length) & element[zaeler] != null)
                    {
                        if (element[zaeler].Geburtsdatum._Geburtsdatum == Daten)
                        {
                            System.Console.WriteLine(element[zaeler].Vorname);
                            System.Console.WriteLine(element[zaeler].Nachname);
                            System.Console.WriteLine(element[zaeler].Geburtsdatum._Geburtsdatum);
                            System.Console.WriteLine(element[zaeler].Versicherungsnummer);
                            gefunden = true;
                        }
                        zaeler++;
                    }
                    break;

                case "Versicherungsnummer":
                    while ((gefunden == false | zaeler < element.Length) & element[zaeler] != null)
                    {
                        if (element[zaeler].Versicherungsnummer == Convert.ToInt32(Daten))
                        {
                            System.Console.WriteLine(element[zaeler].Vorname);
                            System.Console.WriteLine(element[zaeler].Nachname);
                            System.Console.WriteLine(element[zaeler].Geburtsdatum._Geburtsdatum);
                            System.Console.WriteLine(element[zaeler].Versicherungsnummer);
                            gefunden = true;
                        }
                        zaeler++;
                    }
                    break;
                default:
                    System.Console.WriteLine("Existiert nicht oder falsch geschrieben");    //bei fehlerhaften Eingabe
                    break;

            }
        }

        public void print_list()    //Inhalt des Record ausgeben
        {

            int zaeler = 0;

            while (zaeler < element.Length & element[zaeler] != null)
            {
                System.Console.WriteLine(element[zaeler].Vorname);
                System.Console.WriteLine(element[zaeler].Nachname);
                System.Console.WriteLine(element[zaeler].Geburtsdatum._Geburtsdatum);
                System.Console.WriteLine(element[zaeler].Versicherungsnummer);
                zaeler++;
            }
        }



        public void write_list_to_disk(string Dateiname)    //Inhalt des Record in Dateischreiben
        {
            TextWriter tw = new StreamWriter(@"D:\Aufgabe1.txt");

            int zaeler = 0, index = 0;

            while (zaeler < element.Length)
            {
                if (element[zaeler] != null)
                {
                    tw.WriteLine(element[zaeler].Vorname);
                    tw.WriteLine(element[zaeler].Nachname);
                    tw.WriteLine(element[zaeler].Geburtsdatum._Geburtsdatum);
                    tw.WriteLine(element[zaeler].Versicherungsnummer);
                }
                zaeler++;
            }
            tw.Close();
        }

        public Stammdaten[] read_list_from_disk(string Dateiname)   //aus Datei lesen und in Record schreiben
        {
            string Dateiinhalt = "";
            Stammdaten[] Daten = new Stammdaten[10];
            Stammdaten daten = new Stammdaten();
            Datum Geburtsdatum = new Datum();
            int zaeler = 0;
            if (File.Exists(Dateiname))
            {
                StreamReader tw = new StreamReader(Dateiname, System.Text.Encoding.Default);
                while ((Dateiinhalt = tw.ReadLine())!=null) //solange er nicht ans Datei ande gelangt ist Daten in Record schreiben
                {

                    daten.Vorname = Dateiinhalt;
                    Dateiinhalt = tw.ReadLine();
                    daten.Nachname = Dateiinhalt;
                    Dateiinhalt = tw.ReadLine();
                    Geburtsdatum._Geburtsdatum = Dateiinhalt;
                    daten.Geburtsdatum = Geburtsdatum;
                    Dateiinhalt = tw.ReadLine();
                    daten.Versicherungsnummer = Convert.ToInt32(Dateiinhalt);
                    Daten[zaeler] = daten;
                    zaeler++;
                }
                tw.Close();
            }
            System.Console.WriteLine(Dateiinhalt);
            return Daten;
        }

        public void delete_element()
        {
            int erase;
            int zaeler = 0;
            System.Console.Write("Gib zu löschendes Element an: ");
            erase = System.Convert.ToInt32(System.Console.ReadLine());
            Stammdaten[] Daten = new Stammdaten[10];
            int index = 0;
            if (element[erase - 1].next == -1 & element[erase - 1].previous != -1)      //wenn letzter gelöscht werden soll
            {
                element[erase - 2].next = -1;   //setze next auf -1
                element[erase - 1] = null;      //entferne Inhalt aus dem zulöschenden Speicherplatz im record
            }
            else if (element[erase - 1].next != -1 & element[erase - 1].previous != -1)//wenn einer zw. erstem und letzten gelöscht werden soll
            {
                element[erase - 2].next = element[erase - 2].next + 1;      //setze next  vom vorherigen auf den nachfolgenden
                element[erase].previous = element[erase].previous - 1;       //setze previous vom nachvolgenden auf den vorherigen
                element[erase - 1] = null;                                  //entferne Inhalt aus dem zulöschenden Speicherplatz im record
            }
            else if (element[erase - 1].next != -1 & element[erase - 1].previous == -1) //wenn letzter erster werden soll
            {
                element[erase - 1] = null;          //entferne Inhalt aus dem zulöschenden Speicherplatz im record
                element[erase].previous = -1;       //setze previous auf -1
            }
        }
    }



    public class Stammdaten
    {
        public string Vorname, Nachname, Dateiname;
        public int Versicherungsnummer;
        public int previous = -1, next = -1;

        public Datum Geburtsdatum;

        class Program
        {
            static void Main(string[] args)
            {

                Record d = new Record();


                d.insert_element();

                d.insert_element();
                d.print_element();
                d.insert_element();

                d.count_element();
                // d.find_element();
                d.delete_element();
                d.count_element();
                d.write_list_to_disk(@"D:\Aufgabe1.txt");
                d.read_list_from_disk(@"D:\Aufgabe1.txt");
                d.print_list();
                //d.find_element();*/


                System.Console.ReadLine();
            }
        }
    }
}

