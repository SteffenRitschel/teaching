﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ADS_Aufgabe3
{
    class Liste
    {
        Knoten element;

        public void insert_right() //element links von der aktuellen position anfügen wenn bereits elemente existieren ansonsten Wurzel erstellen
        {
            if (element == null)
            {
                element = new Knoten();
            }
            else
            {                                
                Knoten neu = new Knoten();
                neu.previous = element;
                element.right = neu;
            }
        }

        public void insert_left()   //element rechts von der aktuellen position anfügen wenn bereits elemente existieren ansonsten Wurzel erstellen
        {
            if (element == null)
            {
                element = new Knoten();
            }
            else
            {
                Knoten neu = new Knoten();
                neu.previous = element;
                element.left = neu;
            }
        }

        public String insert_String(String _Inhalt)     //Übergebenen String Zeichen weise in Knoten schreiben
        {
            char[] Inarr;
            int i = 0;
            Inarr =_Inhalt.ToCharArray();
                element = new Knoten();
                while (Inarr.Length != i)
                {
                    if ((Inarr[i] == '.') | (Inarr[i] == '_') | (Inarr[i] == ' ') | (Inarr[i] == '?'))  //bei Satz-,Leerzeichen oder Unterstrich zurück zur Wurzel  
                    {                                                                                   //dann soweit links wie möglich und neuen Knoten anhängen
                        while (element.previous != null)
                        {
                            element = element.previous;
                        }
                        while (element.left != null)
                        {
                            element = element.left;
                        }
                        insert_left();
                        element = element.left;
                        element.Inhalt = Inarr[i];
                        i++;
                    }
                    else
                    {
                        while (element.left != null)
                        {
                            element = element.left;
                        }
                        while (element.right != null)
                        {
                            element = element.right;

                        }
                        if (element.Inhalt == '\0')     //wenn Knoten leer füge Inhalt ein
                        {
                            element.Inhalt = Inarr[i];
                            i++;
                        }
                        else
                        {

                            insert_right();
                            element = element.right;
                            element.Inhalt = Inarr[i];
                            i++;
                        }
                    }
                }

                while (element.previous != null)
                    element = element.previous;
            return _Inhalt;
        }
    }

    class Knoten
    {
        public Knoten left, right, previous;
        public char Inhalt, temp;

        public Knoten()
        {
            left = null;
            right = null;
            previous = null;
            
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Liste baum = new Liste();
            baum.insert_String("Dies_ist_ein_binaerer_Baum_zum_traversieren.");
            baum.insert_left();
            baum.insert_right();
            
            System.Console.Read();
            
        }
    }
}
