﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Diagnostics;

namespace ADS_Aufgabe2
{
    class Tastatureingabe       
    {
        int rekursionen = 0;
        List<char> Spooler = new List<char>();
        bool lesen = false;
        
        char[] Inhalt;
        public bool _Eingabe()
        {
            System.ConsoleKeyInfo Eingabe;
                    Eingabe = Console.ReadKey(true);    //Eingabe erscheint nicht in der Console mit true
                    if (Eingabe.KeyChar == '0')         //Wenn 0 eingegeben dann Ausgabe des Spollers leeren und beenden
                    {
                        System.Console.WriteLine();
                        Inhalt = Spooler.ToArray();
                        System.Console.Write(Inhalt);
                        return true;
                    }
                    if (Eingabe.Modifiers != ConsoleModifiers.Control)  // Wenn der Spooler weniger als 20 Einträge hat dann hinzufügen 
                        if (Spooler.Count <= 19)
                        {
                            Spooler.Add(Eingabe.KeyChar);
                        }
                        else System.Console.WriteLine("Spooler ist voll");  //Ansonsten Meldung
                    return false;
        }
        public void _Ausgabe()
        {
                while (true)
                {
                        if (Spooler.Count > 9| lesen==true)     //wenn Spooler 10 Einträge hat Ausgabe beginnen
                        {
                            lesen = true;
                            if (Spooler.Count != 0)
                            {
                                System.Console.WriteLine(Spooler.ElementAt(0)); //jedes Ausgegebenes Element aus dem Spooler löschen
                                Spooler.RemoveAt(0);
                                Thread.Sleep(1000);
                            }
                        }
                    }
        }
        public object euklid(int a, int b)
        {
            if (b == 0) {
                StackTrace stackTrace = new StackTrace();           // hohle Aufruf
                StackFrame[] stackFrames = stackTrace.GetFrames();  // hohle Methoden aufrufe

                // write call stack method names
                foreach (StackFrame stackFrame in stackFrames)
                {
                    Console.WriteLine(stackFrame.GetMethod().Name);   // Methodenamen schreiben
                }

                System.Console.WriteLine("Rekursionen: "+rekursionen);  
                return a; 
            }
            else 
            {
                rekursionen++;
                return euklid(b, a % b); //Rekursion von Euklid
            }   //für Änderung siehe wikipedia da sind noch andere Möglichkeiten für Euklid sind aber alle richtig
        }

        class Program       //Spooler muss abgeändert werden Prof meinte ist seltsam
        {
            static void Main(string[] args)
            {
               
                bool Abbruch;
                Thread lese;
                Tastatureingabe a = new Tastatureingabe();
                lese = new Thread(new ThreadStart(a._Ausgabe));
                lese.Start();
                a.euklid(345,576);
                while (true)
                {
                    Abbruch=a._Eingabe();
                    if (Abbruch == true)
                    {
                        lese.Abort();
                        return;
                    }
                }
            }
        }
    }
}