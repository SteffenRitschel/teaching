import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

class Zeichen
{
	public char buchstabe = 0;
	public int Anzahl;


	public ArrayList<Zeichen> Z�hlen() throws IOException
	{
		int z�hler=65;
		char temp;
		ArrayList<Zeichen> Buchstaben= new ArrayList<Zeichen>();
		String Dateiname = "KHR95_red.txt";
		char Dateiinhalt = 0 ;
		for(int i=0;i<=28;i++){
			Zeichen z = new Zeichen();
			Buchstaben.add(z);
			if(z�hler<91){
				temp= (char)z�hler;
				z.buchstabe=temp;
			}
			else if(z�hler==91)z.buchstabe='�';
			else if(z�hler==92)z.buchstabe='�';
			else if(z�hler==93)z.buchstabe='�';
			z�hler++;
		}

		int codiert=0;
		String zeichen = null;
		BufferedReader buff = new BufferedReader (new FileReader (Dateiname));

		while (codiert!=-1)
		{
			codiert = buff.read();
			if(codiert!=-1){
				Dateiinhalt= (char) codiert;
				zeichen=String.valueOf(Dateiinhalt).toUpperCase();
				if(Buchstaben.contains(zeichen)) { 
					Buchstaben.get(Buchstaben.indexOf(zeichen)).Anzahl++; 
					
				}else{
					Zeichen z = new Zeichen();
					z.Anzahl++;
					z.buchstabe = Dateiinhalt;
					Buchstaben.add(z);
				}
			}
			
			switch(zeichen)
			{
			case "A":
				Buchstaben.get(0).Anzahl++;
				break;
			case "B":
				Buchstaben.get(1).Anzahl++;
				break;
			case "C":
				Buchstaben.get(2).Anzahl++;
				break;
			case "D":
				Buchstaben.get(3).Anzahl++;
				break;
			case "E":
				Buchstaben.get(4).Anzahl++;
				break;
			case "F":
				Buchstaben.get(5).Anzahl++;
				break;
			case "G":
				Buchstaben.get(6).Anzahl++;
				break;
			case "H":
				Buchstaben.get(7).Anzahl++;
				break;
			case "I":
				Buchstaben.get(8).Anzahl++;
				break;
			case "J":
				Buchstaben.get(9).Anzahl++;
				break;
			case "K":
				Buchstaben.get(10).Anzahl++;
				break;
			case "L":
				Buchstaben.get(11).Anzahl++;
				break;
			case "M":
				Buchstaben.get(12).Anzahl++;
				break;
			case "N":
				Buchstaben.get(13).Anzahl++;
				break;
			case "O":
				Buchstaben.get(14).Anzahl++;
				break;
			case "P":
				Buchstaben.get(15).Anzahl++;
				break;
			case "Q":
				Buchstaben.get(16).Anzahl++;
				break;
			case "R":
				Buchstaben.get(17).Anzahl++;
				break;
			case "S":
				Buchstaben.get(18).Anzahl++;
				break;
			case "T":
				Buchstaben.get(19).Anzahl++;
				break;
			case "U":
				Buchstaben.get(20).Anzahl++;
				break;
			case "V":
				Buchstaben.get(21).Anzahl++;
				break;
			case "W":
				Buchstaben.get(22).Anzahl++;
				break;
			case "X":
				Buchstaben.get(23).Anzahl++;
				break;
			case "Y":
				Buchstaben.get(24).Anzahl++;
				break;
			case "Z":
				Buchstaben.get(25).Anzahl++;
				break;
			case "�":
				Buchstaben.get(26).Anzahl++;
				break;
			case "�":
				Buchstaben.get(27).Anzahl++;
				break;
			case "�":
				Buchstaben.get(28).Anzahl++;
				break;
			}
		}
		buff.close();
		return Buchstaben;	
	}

	public void Huffmann_Baum()
	{
		int l�sch_index=0;
		try {
			List<Huffman> huffman = new ArrayList<Huffman>();
			List<Zeichen> Zeichen = Z�hlen();
			while(Zeichen.size()!=0){
				Zeichen tempzeichen=Zeichen.get(0);
				for(int i=0;i<Zeichen.size();i++){
					if(tempzeichen.Anzahl>Zeichen.get(i).Anzahl){
						tempzeichen=Zeichen.get(i);
						l�sch_index=i;
					}
				}
				Zeichen.remove(l�sch_index);
				Huffman huff_left = new Huffman();
				huff_left.zeichen=tempzeichen;
				tempzeichen=Zeichen.get(0);
				l�sch_index=0;
				for(int i=0;i<Zeichen.size();i++){
					if(tempzeichen.Anzahl>Zeichen.get(i).Anzahl){
						tempzeichen=Zeichen.get(i);
						l�sch_index=i;
					}
				}

				Zeichen.remove(l�sch_index);
				Huffman huff_right = new Huffman();
				huff_right.zeichen=tempzeichen;
				Huffman knoten = new Huffman();
				knoten.left=huff_left;
				huff_left.previous=knoten;
				huff_right.zeichen=tempzeichen;
				knoten.right=huff_right;
				huff_right.previous=knoten;
				knoten.sum=huff_left.zeichen.Anzahl+huff_right.zeichen.Anzahl;
				huffman.add(knoten);

				if(Zeichen.size()==1){
					knoten= new Huffman();
					knoten.zeichen=Zeichen.get(0);

					huffman.add(knoten);
					Zeichen.remove(0);
				}
			}
			while(huffman.size()!=1){
				huffman=baum_bauen(huffman);
			}


		}catch (IOException e) {
			e.printStackTrace();
		}
	}

	private List<Huffman> baum_bauen(List<Huffman> huffman){

		List<Huffman> baum=new ArrayList<Huffman>();
		while(huffman.size()!=0)
		{
			if(huffman.size()!=1){
				Huffman knoten = new Huffman();
				knoten.left=huffman.get(0);
				huffman.get(0).previous=knoten;
				knoten.right=huffman.get(1);;
				huffman.get(1).previous=knoten;
				knoten.sum=huffman.get(0).sum+huffman.get(1).sum;
				baum.add(knoten);
				huffman.remove(0);
				huffman.remove(0);
			}else{
				Huffman knoten= new Huffman();
				knoten.zeichen=huffman.get(0).zeichen;
				knoten.sum=huffman.get(0).zeichen.Anzahl;
				baum.add(knoten);
				huffman.remove(0);
			}
		}
		return baum;
	}
}