import java.util.ArrayList;

public class Huffman {
	Zeichen zeichen;
	Huffman previous;
	Huffman left;
	Huffman right;
	int sum;
	
	@SuppressWarnings("unused")
	public Huffman()
	{
		Huffman previous=null;
		Huffman left=null;
		Huffman right=null;
	}
	
	//ich wei� nicht wie die folgenden Methoden genau arbeiten versuche aber trotzdem so gut wie m�glich diese zu beschreiben falls mein Beschreibung 
	//falsch sein mag bitte ich denjenigen der es besser wei� als ich die Kommentierung zu verbessern
	
	//Quuelle: http://algs4.cs.princeton.edu/55compression/Huffman.java.html
	
	private boolean isLeaf() {		//pr�fen ob es ein baum ist
        assert (left == null && right == null) || (left != null && right != null);		//pr�fe ob left && right null oder nicht null sind
        return (left == null && right == null);
    }
	
	 public static void buildCode(ArrayList<Code> st, Huffman x, String s,Code d) {
	        if (!x.isLeaf()) {							//wenn es kein Blatt ist gehe nach links und schreibe dabei den Code bis er zum Blatt kommt 
	            buildCode(st, x.left,  s + '0',d);		//dann f�ge erstellten Code in die Liste ein
	            buildCode(st, x.right, s + '1',d);		// diese Zeile wird erst bearbeitet wenn er denn ersten Code erstellt hat dann besuche das Blatt rechts vom aktuellen
	        }											//m�glich dadurch weil in 'x' referenced from die beiden Bl�tter und  der vorletzte Knoten enthalten sind
	        else {										
	        	d= new Code();
	        	d.code=s;
	        	d.Buchstabe=x.zeichen.buchstabe;		
	            st.add(d);								//schreiben des Codes und des dazugeh�rigen Buchstaben in eine Liste
	        }
	    }

}
