import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

class Zeichen {
	public String buchstabe = "";
	public int Anzahl;

	public ArrayList<Zeichen> Z�hlen() throws IOException {
		String Dateiname = "KHR95_red.txt";
		char Dateiinhalt = 0;
		int codiert = 0;
		String zeichen = null;

		ArrayList<String> Buchstaben = new ArrayList<String>();
		ArrayList<Zeichen> Zeichen = new ArrayList<Zeichen>();
		BufferedReader buff = new BufferedReader(new FileReader(Dateiname));

		while (codiert != -1) {												//solange Dateiende nicht erreicht wurde
			codiert = buff.read();											//Zeichenweise lesen 
			if (codiert != -1) {											//wenn Datei ende noch nicht erreicht;
				Dateiinhalt = (char) codiert;								//Wert in ein nach ACII-Zeichen umwandeln
				zeichen = String.valueOf(Dateiinhalt).toUpperCase();		//ignorieren von Kleinschreibung
				//zeichen = String.valueOf(Dateiinhalt);					//Beachten von Gro�- und Kleinschreibung
				if (Character.isLetter(Dateiinhalt))						//wenn es ein Buchstabe ist
					if (Buchstaben.contains(zeichen)) {						//wenn es schon in der Liste ist
						Zeichen.get(Buchstaben.indexOf(zeichen)).Anzahl++;	//Anzahl um 1 erh�hen

					} else {												//ansonsten neuen Listeneintrag mit dem Buchstaben und Anzahl von 1
						Zeichen list = new Zeichen();
						Buchstaben.add(zeichen);
						list.buchstabe = zeichen;
						list.Anzahl = 1;
						Zeichen.add(list);
					}
			}
		}
		int zaehler = 0;													
		while (Zeichen.size() > zaehler) {									//Ausgabe der gefundenen Zeichen und ihrer H�ufigkeit
			System.out.println(Zeichen.get(zaehler).buchstabe + "= "
					+ Zeichen.get(zaehler).Anzahl);
			zaehler++;
		}
		buff.close();				
		return Zeichen;
	}																		
																			  																		  
	public Huffman Huffmann_Baum() {				
		int l�sch_index = 0;												//zum entfernen von eintr�gen die in einen Knoten zusammengef�hrt werden
		List<Huffman> huffman = new ArrayList<Huffman>();
		try {

			List<Zeichen> Zeichen = Z�hlen();								//Liste mit den Buchstaben und ihren H�ufigketen
			while (Zeichen.size() != 0) {									//solange nicht alle Zeichen in einen Knoten zusammgef�hrt wurden
				l�sch_index = 0;
				Zeichen tempzeichen = Zeichen.get(0);						//ersten Buchstaben aus der Liste holen
				for (int i = 0; i < Zeichen.size(); i++) {					//Vergleich der H�ufigkeiten bis geringsten gefunden wurde
					if (tempzeichen.Anzahl > Zeichen.get(i).Anzahl) {
						tempzeichen = Zeichen.get(i);						//Zwischenspeichern der H�figkeit 
						l�sch_index = i;
					}
				}
				Zeichen.remove(l�sch_index);								//entfernen der geringsten H�ufigkeit aus der Liste
				Huffman huff_left = new Huffman();
				huff_left.zeichen = tempzeichen;							
				tempzeichen = Zeichen.get(0);								//wieder ersten eintrag holen
				l�sch_index = 0;
				for (int i = 0; i < Zeichen.size(); i++) {					//Vergleich der H�ufigkeiten bis geringsten gefunden wurde
					if (tempzeichen.Anzahl > Zeichen.get(i).Anzahl) {
						tempzeichen = Zeichen.get(i);
						l�sch_index = i;
					}
				}

				Zeichen.remove(l�sch_index);								//entfernen der geringsten H�ufigkeit aus der Liste
				Huffman huff_right = new Huffman();							
				huff_right.zeichen = tempzeichen;
				Huffman knoten = new Huffman();
				knoten.left = huff_left;									//links anf�gen
				huff_left.previous = knoten;
				huff_right.zeichen = tempzeichen;
				knoten.right = huff_right;									//rechts anf�gen
				huff_right.previous = knoten;								
				knoten.sum = huff_left.zeichen.Anzahl						//Knotensumme berechnen
						+ huff_right.zeichen.Anzahl;
				huffman.add(knoten);

				if (Zeichen.size() == 1) {								//wenn ein Buchstabe f�r sich �brigbleibt
					knoten = new Huffman();
					knoten.zeichen = Zeichen.get(0);					//in ein Knoten packen
					huffman.add(knoten);								//und letzten Eintrag in der Liste l�schen
					Zeichen.remove(0);
				}
			}
			while (huffman.size() != 1) {								//wiederholen bis der Huffmannbaum fertig ist
				huffman = baum_bauen(huffman);
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
		Huffman ret = huffman.get(0);
		return ret;
	}

	private List<Huffman> baum_bauen(List<Huffman> huffman) {		//Eintr�ge in der Liste sind der gr��e aufsteigend geordnet

		List<Huffman> baum = new ArrayList<Huffman>();			
		while (huffman.size() != 0) {				//solange in �bergebene Liste Eintr�ge sind
			if (huffman.size() != 1) {				//solange in neuer Liste mehr als ein Eintrag ist
				Huffman knoten = new Huffman();		
				knoten.left = huffman.get(0);		//anf�gen des ersten Knotens aus der Liste an einen neuen
				huffman.get(0).previous = knoten;
				knoten.right = huffman.get(1);		//anf�gen des zweiten Knotens aus der Liste 
				;
				huffman.get(1).previous = knoten;
				knoten.sum = huffman.get(0).sum + huffman.get(1).sum;	//berechnen der neuen Summe
				baum.add(knoten);
				huffman.remove(0);					//angef�gte Knoten aus der �bergebenen Liste  l�schen
				huffman.remove(0);
			} else {
				Huffman knoten = new Huffman();					//wenn nur noch ein einzelner Eintrag ist
				knoten.zeichen = huffman.get(0).zeichen;		//in neuen Knoten Speichern
				knoten.sum = huffman.get(0).zeichen.Anzahl;		//summe setzen
				baum.add(knoten);								//liste speichern
				huffman.remove(0);								//letzen eintrag l�schen
			}
		}
		return baum;
	}
}