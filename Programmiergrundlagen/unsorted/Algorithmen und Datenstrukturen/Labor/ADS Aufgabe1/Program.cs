﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace ADS_Aufgabe2
{

    public class Datum      //Klasse fürs Datum(eigentlich nicht notwendig denn C# besitzt was eigenes dafür)
    {
        private String Geburtsdatum;
        public String _Geburtsdatum
        {
            get{return Geburtsdatum;}
            set { Geburtsdatum = value;}
        }
    }

    public class Liste 
    {
        Stammdaten element;
        public void insert_element()    //einfügen eines Elements
        {
            if (element == null)        //wenn noch keins existiert
            {
                element = new Stammdaten();
                Datum Geburtsdatum = new Datum();
                System.Console.Write("Vorname: ");
                element.Vorname = System.Console.ReadLine();
                System.Console.Write("Nachname: ");
                element.Nachname = System.Console.ReadLine();
                System.Console.Write("Geburtstag: ");
                Geburtsdatum._Geburtsdatum = System.Console.ReadLine();
                element.Geburtsdatum = Geburtsdatum;
                System.Console.Write("Versicherungsnummer: ");
                element.Versicherungsnummer = System.Convert.ToInt32(System.Console.ReadLine());
                
            }
            else {      //wenn besreits welche existieren

            while (element.next != null)        //gehe ans ende der Liste und füge an
            {
                element = element.next;
            }
                Stammdaten neu = new Stammdaten();
                Datum Geburtsdatum = new Datum();
                
                System.Console.Write("Vorname: ");
                neu.Vorname = System.Console.ReadLine();
                System.Console.Write("Nachname: ");
                neu.Nachname = System.Console.ReadLine();
                System.Console.Write("Geburtstag: ");
                Geburtsdatum._Geburtsdatum = System.Console.ReadLine();
                neu.Geburtsdatum = Geburtsdatum;
                System.Console.Write("Versicherungsnummer: ");
                neu.Versicherungsnummer = System.Convert.ToInt32(System.Console.ReadLine());
                

                neu.preview = element;
                element.next = neu;
            }
        }

        public void print_element()     //Ausgabe eines elements
        {
            while (element.preview != null) { element = element.preview; }      //gehe zum ersten element
            System.Console.Write("Gib Element an das ausgegeben werden soll: ");
            int Eingabe = Convert.ToInt32(System.Console.ReadLine());           //eingabe in int umwandeln
            try
            {
                for (int i = 1; i < Eingabe; i++)       //zum element gehen
                { element = element.next; }
            }
            catch (Exception e) { System.Console.Write("Angegebenes Element existiert nicht."); }
            if (this.element != null)
            {
                try           //Ausgabe
                {       
                    System.Console.WriteLine("{0}", this.element.Vorname);
                    System.Console.WriteLine("{0}", this.element.Nachname);
                    System.Console.WriteLine("{0}", this.element.Geburtsdatum._Geburtsdatum);
                    System.Console.WriteLine("{0}", this.element.Versicherungsnummer);
                }
                catch (Exception e) { System.Console.WriteLine("Es fehlen Daten."); }
            }
            else { System.Console.WriteLine("Es sind keine Daten vorhanden"); }
        }

        public void count_element()
        {
            int zaeler = 1;
            while (element.preview != null) //gehe aufs erste element
            { element = element.preview; }

            while (element.next != null)    //zähle durch
            {
                element = element.next;
                zaeler++;
            }
            System.Console.WriteLine("Anzahl:{0}", zaeler);
        }

        public void find_element()  //bei mir nur suche nach Versicherungsnummer
        {
            int zaeler, VN;

            while (element.preview != null) //gehe aufs erste element
            { element = element.preview; }

            System.Console.Write("Gib Versicherungsnummer an: ");   
            VN = Convert.ToInt32(System.Console.ReadLine());
            for (zaeler = 0; element != null; zaeler++)     //elemente der Liste durchgehen
            {
                if (element.Versicherungsnummer == VN)
                {
                    System.Console.WriteLine("{0}", this.element.Vorname);
                    System.Console.WriteLine("{0}", this.element.Nachname);
                    System.Console.WriteLine("{0}", this.element.Geburtsdatum._Geburtsdatum);
                    System.Console.WriteLine("{0}", this.element.Versicherungsnummer);
                    element = null;
                }
                else { element = element.next; }
            }
        }

        public void print_list()
        {
            while (element.preview != null) //gehe aufs erste element
            { element = element.preview; }
            Stammdaten temp;
            temp = element;
            while (temp.next != null)
            {
                try           //Ausgabe
                {
                    System.Console.WriteLine("{0}", this.element.Vorname);
                    System.Console.WriteLine("{0}", this.element.Nachname);
                    System.Console.WriteLine("{0}", this.element.Geburtsdatum._Geburtsdatum);
                    System.Console.WriteLine("{0}", this.element.Versicherungsnummer);
                }
                catch (Exception e) { System.Console.WriteLine("Es fehlen Daten."); }
                temp = temp.next;
                zaeler++;
            }
        }

        public void write_list_to_disk(string Dateiname)
        {
            TextWriter tw = new StreamWriter(@"E:\Aufgabe1.txt");
            while (element.preview != null) //gehe aufs erste element
            { element = element.preview; }

            int zaeler = 1;
            int vergleich = 0;
            Stammdaten temp;
            temp = element;
            while (temp.next != null)   //zähle elemente
            {
                temp = temp.next;
                zaeler++;
            }

            while (vergleich != zaeler)    //schreibe in die Datei solange er nicht so oft wiederholt hat wie elemente in der Liste sind
            {                             
                tw.WriteLine(element.Vorname);
                tw.WriteLine(element.Nachname);
                tw.WriteLine(element.Geburtsdatum._Geburtsdatum);
                tw.WriteLine(element.Versicherungsnummer);
                tw.WriteLine("");
                if(element.next!=null)
                element = element.next;
                vergleich++;
            }
            tw.Close();
        }

        public string read_list_from_disk(string Dateiname)
        {
            string Dateiinhalt = "";
            Datum Geburtsdatum = new Datum();
            Stammdaten read= new Stammdaten();
            if (File.Exists(Dateiname))
            {
                StreamReader tw = new StreamReader(Dateiname, System.Text.Encoding.Default);
                do      //schreibe in Liste solange es noch weitere gibt in der Datei gibt
                {
                    Stammdaten temp=new Stammdaten();
                    Dateiinhalt = tw.ReadLine();
                    System.Console.WriteLine(Dateiinhalt);
                    temp.Vorname = Dateiinhalt;
                    Dateiinhalt = tw.ReadLine();
                    System.Console.WriteLine(Dateiinhalt);
                    temp.Nachname = Dateiinhalt;
                    Dateiinhalt = tw.ReadLine();
                    System.Console.WriteLine(Dateiinhalt);
                    Geburtsdatum._Geburtsdatum = Dateiinhalt;
                    temp.Geburtsdatum = Geburtsdatum;
                    Dateiinhalt = tw.ReadLine();
                    temp.Versicherungsnummer = Convert.ToInt32(Dateiinhalt);
                    if (read.next == null) { read = temp; } //wenn erstes elementt dann füge hinzu
                    else if (read.next != null)     //wenn nicht 
                    {
                        while (read.next != null)   //geh zum letzten element
                        {
                            read = read.next;
                        }
                        read.next = temp;           //und füg ans ende hinzu
                        temp.preview = read;
                    }
                    Dateiinhalt=tw.ReadLine();      //lese Leerzeile zwischen zwei Personen
                    Dateiinhalt = tw.ReadLine();    //lese anfang von der nächsten Person

                    
                } while (Dateiinhalt != null);
                tw.Close();
            }
            return Dateiinhalt;
        }

        public void delete_element()
        {
            int t;
            int zaeler = 0;
            while (element.preview != null) //gehe aufs erste element
            { element = element.preview; }
            System.Console.Write("Gib zu löschendes Element an: ");
            t = System.Convert.ToInt32(System.Console.ReadLine());

            while (zaeler != t)             //geh zum zu löschenden Element
            {
                zaeler++;
                element = element.next;
            }
            if (element.next == null)       //lösch letztes element
            {
                element = element.preview;
                element.next = null;
            }
            else
            {
                if (element.preview == null)//lösch erstes element
                {
                    element = element.next;
                    element.preview = null;
                }
                else
                {
                    element.preview.next = element.next;//lösch ein element zw. erstem und letzten
                    element.next.preview = element.preview;
                }
            }
        }   
    }


    public class Stammdaten     //Daten der Person
    {
        public int l = 0;
        public string Vorname, Nachname;
        public int Versicherungsnummer;
        public Stammdaten preview;
        public Stammdaten next;
        public Datum Geburtsdatum;
    }

    class Program
    {
        static void Main(string[] args)
        {

            Liste d = new Liste();
           d.insert_element();
          
          d.insert_element();
           d.print_element();
           d.count_element();
         d.delete_element();
           d.count_element();
         d.write_list_to_disk(@"E:\Aufgabe1.txt");
         d.read_list_from_disk(@"E:\Aufgabe1.txt");
       // c.print_list(c);
         d.find_element();
            System.Console.ReadLine();
        }
    }
}