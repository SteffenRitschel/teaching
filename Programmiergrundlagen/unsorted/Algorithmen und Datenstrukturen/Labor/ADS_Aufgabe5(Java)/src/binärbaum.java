import java.util.ArrayList;
import java.util.List;


public class bin�rbaum {

	String Vorname = null;
	int Knotenindex = 0;
	bin�rbaum left, previous, right;

	public bin�rbaum() {
		left = null;
		previous = null;
		right = null;
	}

	public bin�rbaum normale_Reihenfolge(String[] Vornamen) {
		bin�rbaum baum = new bin�rbaum();
		baum.Vorname = Vornamen[0];
		int index_gesucht = 0, index = 0;
		char[] bin�rezahl = null;
		//---------------------------------------------------------------------------------------------
		while (Vornamen.length != index - 1) {	
			if (index_gesucht == (baum.Knotenindex)) {	//wenn gesuchter Knoten gefunden Knoten anf�gen mit Vornamen
				if (baum.left == null & index + 1 != Vornamen.length) {	//links einen Knoten anf�gen
					bin�rbaum neu = new bin�rbaum();
					neu.previous = baum;
					baum.left = neu;
					baum = baum.left;
					index++;
					baum.Knotenindex = index;
					baum.Vorname = Vornamen[index];
					baum = baum.previous;
				}
				//---------------------------------------------------------------------------------------------
				if (baum.right == null & index + 1 != Vornamen.length) {	//links einen Knoten anf�gen
					bin�rbaum neu = new bin�rbaum();
					neu.previous = baum;
					baum.right = neu;
					baum = baum.right;
					index++;
					baum.Knotenindex = index;
					baum.Vorname = Vornamen[index];
					baum = baum.previous;
				}
				//---------------------------------------------------------------------------------------------
				if (index + 1 == Vornamen.length)	//wenn alle Namen abgelegt Methode verlassen
					break;
				//---------------------------------------------------------------------------------------------
				index_gesucht++;
				while (baum.previous != null)		//zur�ck zur Wurzel
					baum = baum.previous;

			} else {
				boolean gefunden = false;
				int i = 1, left_height = 0;
				//---------------------------------------------------------------------------------------------
				while (baum.left != null) {		//H�he bestimmen
					baum = baum.left;
					left_height++;
					if (baum.Knotenindex == index_gesucht) { //wenn Knoten gefunden Schleife verlassen
						gefunden = true;
						break;
					}
				}
				//---------------------------------------------------------------------------------------------
				while (baum.previous != null & gefunden == false)	//zur�ck zur Wurzel falls Knoten noch nicht gefunden
					baum = baum.previous;
				if (baum.right != null)
					baum = baum.right;
				//---------------------------------------------------------------------------------------------
				if (baum.Knotenindex == index_gesucht) {
					gefunden = true;
				}
				// -----------------------------------------------------------------------------------------
				while (baum.left != null & gefunden == false) {		//nach ganz links
					baum = baum.left;
					if (baum.Knotenindex == index_gesucht) {
						gefunden = true;
						break;
					}
				}
				// -----------------------------------------------------------------------------------------
				while (baum.previous != null & gefunden == false)	// zur�ck zur Wurzel
					baum = baum.previous;
				// -----------------------------------------------------------------------------------------
				bin�rezahl = new char[left_height];		//Suchweite bestimmen
				int a = 1;
				while (bin�rezahl.length != a - 1) {	//suche mit ganz links beginnen
					bin�rezahl[a - 1] = '0';
					a++;
				}
				// -----------------------------------------------------------------------------------------
				int bitz�hler1 = 0, bitz�hler2 = 0, bitz�hler3 = 0, bitz�hler4 = 0;
				while (gefunden == false) {								//solange suchen bis Knoten gefunden
					while (baum.left != null | baum.right != null) {

						if (bin�rezahl[bin�rezahl.length - i] == '0') {	//linken Knoten durchsuchen
							if (baum.left != null)
								baum = baum.left;
							if (baum.Knotenindex == index_gesucht) {
								gefunden = true;
								break;
							}
						}
						if (bin�rezahl[bin�rezahl.length - i] == '1') {	//rechten Knoten durchsuchen
							if (baum.right != null)
								baum = baum.right;
							if (baum.Knotenindex == index_gesucht) {
								gefunden = true;
								break;
							}
						}
						i++;
					}
					i = 1;
					//----------------------------------------------------------------------------------------------
					if (bin�rezahl[0] == '0') {		//neuen Suchpfad bestimmen
						bin�rezahl[0] = '1';
					} else {
						bin�rezahl[0] = '0';
					}
					// ---------------------------------------------------------------------------------------------
					if (bin�rezahl.length > 1) {	//wenn Suche mehr als 1 Schritt enth�lt
						bitz�hler1++;
						if (bitz�hler1 == 2)	//bei jedem 2 mal Suchindex 2 �ndern
							if (bin�rezahl[1] == '0') {
								bin�rezahl[1] = '1';
								bitz�hler1 = 0;
							} else {
								bin�rezahl[1] = '0';
								bitz�hler1 = 0;
							}
					}
					// ---------------------------------------------------------------------------------------------
					if (bin�rezahl.length > 2) {	//wenn Suche mehr als 2 Schritt enth�lt	
						bitz�hler2++;
						if (bitz�hler2 == 4)	//bei jedem 4 mal Suchindex 2 �ndern
							if (bin�rezahl[2] == '0') {
								bin�rezahl[2] = '1';
								bitz�hler2 = 0;
							} else {
								bin�rezahl[2] = '0';
								bitz�hler2 = 0;
							}
					}
					// ---------------------------------------------------------------------------------------------
					if (bin�rezahl.length > 3) {	//wenn Suche mehr als 3 Schritt enth�lt
						bitz�hler3++;
						if (bitz�hler3 == 8)		//bei jedem 8 mal Suchindex 2 �ndern
							if (bin�rezahl[3] == '0') {
								bin�rezahl[3] = '1';
								bitz�hler3 = 0;
							} else {
								bin�rezahl[3] = '0';
								bitz�hler3 = 0;
							}
					}
					// ---------------------------------------------------------------------------------------------
					if (bin�rezahl.length > 4) {	//wenn Suche mehr als 4 Schritt enth�lt
						bitz�hler4++;
						if (bitz�hler4 == 16)		//bei jedem 16 mal Suchindex 2 �ndern
							if (bin�rezahl[4] == '0') {
								bin�rezahl[4] = '1';
								bitz�hler4 = 0;
							} else {
								bin�rezahl[4] = '0';
								bitz�hler4 = 0;
							}
					}
					// ---------------------------------------------------------------------------------------------
					while (baum.previous != null & gefunden == false)	//zur�ck zur Wurzel
						baum = baum.previous;
				}
			}
		}
		return baum;
	}


	public bin�rbaum hash_Reihenfolge(String[] Vornamen) {

		int bitz�hler1 = 0, bitz�hler2 = 0, bitz�hler3 = 0, bitz�hler4 = 0;
		int index_gesucht = 0, index = 0;
		char[] bin�rezahl = null;
		bin�rbaum baum = new bin�rbaum();
		baum.Vorname=Vornamen[0];
		baum.Vorname = Vornamen[index];
		//-------------------------------------------------------------------------------
		while (Vornamen.length != index - 1) {
			if (index_gesucht == (baum.Knotenindex)) {	//wenn gesuchter Knoten gefunden Knoten anf�gen mit Vornamen
				if (baum.left == null & index + 1 != Vornamen.length) {	//links Knoten anf�gen
					bin�rbaum neu = new bin�rbaum();
					neu.previous = baum;
					baum.left = neu;
					baum = baum.left;
					index++;
					baum.Knotenindex = index;
					baum.Vorname = Vornamen[index];
					baum = baum.previous;
				}
				if (baum.right == null & index + 1 != Vornamen.length) {	//rechts Knoten anf�gen
					bin�rbaum neu = new bin�rbaum();
					neu.previous = baum;
					baum.right = neu;
					baum = baum.right;
					index++;
					baum.Knotenindex = index;
					baum.Vorname = Vornamen[index];
					baum = baum.previous;
				}
				index_gesucht++;
				if(index+1==Vornamen.length)	//wenn alle Vornamen abgelegt Methode verlassen
					break;
				while (baum.previous != null)
					baum = baum.previous;
			} else {
				boolean gefunden = false;
				int i = 1, left_height = 0;
				// -----------------------------------------------------------------------------------------
				while (baum.left != null) {	//nach ganz links
					baum = baum.left;
					left_height++;
					if (baum.Knotenindex == index_gesucht) {	
						gefunden = true;
						break;
					}
				}
				// -----------------------------------------------------------------------------------------
				while (baum.previous != null & gefunden == false)	//zur�ck zur Wurzel falls Knoten noch nicht gefunden
					baum = baum.previous;
				if (baum.right != null)	
					baum = baum.right;
				if (baum.Knotenindex == index_gesucht) {
					gefunden = true;
				}
				// -----------------------------------------------------------------------------------------
				while (baum.left != null & gefunden == false) {	//nach ganz links
					baum = baum.left;
					// -----------------------------------------------------------------------------------------
					if (baum.Knotenindex == index_gesucht) {
						gefunden = true;
						break;
					}
				}
				// -----------------------------------------------------------------------------------------
				while (baum.previous != null & gefunden == false)	//zur�ck zur Wurzel
					baum = baum.previous;
				// -----------------------------------------------------------------------------------------
				bin�rezahl = new char[left_height];	//Suchweite bestimmen
				int a = 1;
				while (bin�rezahl.length != a - 1) {//Suche ganz links bestimmen
					bin�rezahl[a - 1] = '0';
					a++;
				}
				while (gefunden == false) {		//solange suchen bis Knoten gefunden
					while ((baum.left != null) |(baum.right != null)) {

						if (bin�rezahl[bin�rezahl.length - i] == '0') {	//linken Knoten pr�fen
							if (baum.left != null)
								baum = baum.left;
							if (baum.Knotenindex == index_gesucht) {
								gefunden = true;
								break;
							}
						}
						if (bin�rezahl[bin�rezahl.length - i] == '1') {	//rechten Knoten pr�fen
							if (baum.right != null)
								baum = baum.right;
							if (baum.Knotenindex == index_gesucht) {
								gefunden = true;
								break;
							}
						}
						i++;
					}
					i = 1;
					if (bin�rezahl[0] == '0') {		//neuen Suchpfad bestimmen
						bin�rezahl[0] = '1';
					} else {
						bin�rezahl[0] = '0';
					}
					// ---------------------------------------------------------------------------------------------
					if (bin�rezahl.length > 1) {	//wenn Suche mehr als 1 Schritt enth�lt
						bitz�hler1++;
						if (bitz�hler1 == 2)	//bei jedem 2 mal Suchindex 2 �ndern
							if (bin�rezahl[1] == '0') {
								bin�rezahl[1] = '1';
								bitz�hler1 = 0;
							} else {
								bin�rezahl[1] = '0';
								bitz�hler1 = 0;
							}
					}
					// ---------------------------------------------------------------------------------------------
					if (bin�rezahl.length > 2) {	//wenn Suche mehr als 2 Schritt enth�lt	
						bitz�hler2++;
						if (bitz�hler2 == 4)	//bei jedem 4 mal Suchindex 2 �ndern
							if (bin�rezahl[2] == '0') {
								bin�rezahl[2] = '1';
								bitz�hler2 = 0;
							} else {
								bin�rezahl[2] = '0';
								bitz�hler2 = 0;
							}
					}
					// ---------------------------------------------------------------------------------------------
					if (bin�rezahl.length > 3) {	//wenn Suche mehr als 3 Schritt enth�lt
						bitz�hler3++;
						if (bitz�hler3 == 8)		//bei jedem 8 mal Suchindex 2 �ndern
							if (bin�rezahl[3] == '0') {
								bin�rezahl[3] = '1';
								bitz�hler3 = 0;
							} else {
								bin�rezahl[3] = '0';
								bitz�hler3 = 0;
							}
					}
					// ---------------------------------------------------------------------------------------------
					if (bin�rezahl.length > 4) {	//wenn Suche mehr als 4 Schritt enth�lt
						bitz�hler4++;
						if (bitz�hler4 == 16)		//bei jedem 16 mal Suchindex 2 �ndern
							if (bin�rezahl[4] == '0') {
								bin�rezahl[4] = '1';
								bitz�hler4 = 0;
							} else {
								bin�rezahl[4] = '0';
								bitz�hler4 = 0;
							}
					}
					// ---------------------------------------------------------------------------------------------
					while (baum.previous != null & gefunden == false)	//zur�ck zur Wurzel
						baum = baum.previous;
				}
			}
		}
		return baum;
	}
	public void inorder(bin�rbaum baum)	//!!f�r die Erstellung des bin�ren Baumes l�sst
	{									//!!sich die Traversieung verwenden muss aber erst implementiert werden
		int h�he=0,temp=0;
		List<String> Knoteninhalt = new ArrayList<String>();
		List<Integer> Knotenindex = new ArrayList<Integer>();
		while(baum.previous!= null)
			baum=baum.previous;
		while(baum.left!= null)	//linken Teilbaum besuchen
		{
			h�he++;
			baum = baum.left;
			if(baum.left==null) {	//Knoteninhalt ganz links speichern
				Knoteninhalt.add(baum.Vorname);
				Knotenindex.add(baum.Knotenindex);

			}
		}
		while(baum.previous!=null)	//zur�ck zur Wurzel 
		{
			if(baum.right!=null ) {	//nach rechts; Knoteninhalt ganz speichern
				if(!(Knotenindex.contains(baum.right.Knotenindex))) {//falls Knoten bereits besucht nicht nochmal besuchen
					if(!(Knotenindex.contains(baum.Knotenindex))) {	//falls Knoten bereits besucht Inhalt nicht speichern
						Knoteninhalt.add(baum.Vorname);
						Knotenindex.add(baum.Knotenindex);
					}
					baum = baum.right;
					Knoteninhalt.add(baum.Vorname);
					Knotenindex.add(baum.Knotenindex);
					while(baum.left!=null) {	//linken Knoten besuchen und Inhalt speichern
						baum=baum.left;
						Knoteninhalt.add(baum.Vorname);
						Knotenindex.add(baum.Knotenindex);
					}
				}
			}else if (baum.right==null & (!(Knotenindex.contains(baum.Knotenindex)))){
				Knoteninhalt.add(baum.Vorname);
				Knotenindex.add(baum.Knotenindex);
			}
			baum=baum.previous;	//vorherigen Knoten besuchen
		}
		Knoteninhalt.add(baum.Vorname);
		Knotenindex.add(baum.Knotenindex);
		
		//----------------------------------------------------------------------------------------------------------------
		while(baum.right!= null){	//am weitesten rechten Knotenindex speichern
			baum=baum.right;
		}
		temp=baum.Knotenindex;		

		while(baum.previous!= null){//zur�ck zur Wurzel
			baum=baum.previous;
		}
		while(baum.Knotenindex!=temp)	//rechten Teilbaum besuchen bis am weitesten rechten Knoten erreicht
		{
			if(baum.left!=null ) {	//linken Knoten besuchen wenn nicht bereits besucht
				if(!(Knotenindex.contains(baum.left.Knotenindex))) {
					if(!(Knotenindex.contains(baum.Knotenindex))) {	//linken Knoteninhalt speichern wenn nicht bereits besucht
						Knoteninhalt.add(baum.Vorname);
						Knotenindex.add(baum.Knotenindex);
					}
					while(baum.left!=null){	//linke Knoten besuchen und Knoteninhalt speichern
						baum = baum.left;
						Knoteninhalt.add(baum.Vorname);
						Knotenindex.add(baum.Knotenindex);
					}
					while(baum.right!=null) {	//rechte Knoten besuchen

						baum=baum.right;
						if (!(Knotenindex.contains(baum.right.Knotenindex))) {	//wenn bereits besucht Knoteninhlat nicht speichern
							Knoteninhalt.add(baum.Vorname);
							Knotenindex.add(baum.Knotenindex);}
					}
				}
			}
			if(baum.right!=null){	//rechten Knoten besuchen
				if(!(Knotenindex.contains(baum.right.Knotenindex))){	//wenn bereits besucht nicht nochmal und ein Knoten zur�ck
					baum=baum.right;
					Knoteninhalt.add(baum.Vorname);
					Knotenindex.add(baum.Knotenindex);
				}else baum=baum.previous;	
			}
			else
				baum=baum.previous;
		}
		System.out.println(Knoteninhalt);
		System.out.println("Baumh�he: "+h�he);

	}

}