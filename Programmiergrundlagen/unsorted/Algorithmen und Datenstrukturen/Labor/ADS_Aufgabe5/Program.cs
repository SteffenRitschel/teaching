﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ADS_Aufgabe5
{
    class binärbaum
    {
        String Vorname = null;
        int Knotenindex = 0;
        binärbaum left, previous, right;
        public binärbaum()
        {
            left = null;
            previous = null;
            right = null;
        }

        public void normale_Reihenfolge(String[] Vornamen)
        {
            binärbaum baum = new binärbaum();
            baum.Vorname = Vornamen[0];
            int index_gesucht = 0, zaehler = 0, index = 0, größe = 0;


            while (Vornamen.Length != zaehler)
            {
                if (index_gesucht == (baum.Knotenindex))
                {
                    if (baum.left == null)
                    {
                        binärbaum neu = new binärbaum();
                        neu.previous = baum;
                        baum.left = neu;
                        baum = baum.left;
                        index++;
                        baum.Knotenindex = index;
                        baum.Vorname = Vornamen[index];
                        baum = baum.previous;
                    }
                    if (baum.right == null)
                    {
                        binärbaum neu = new binärbaum();
                        neu.previous = baum;
                        baum.right = neu;
                        baum = baum.right;
                        index++;
                        baum.Knotenindex = index;
                        baum.Vorname = Vornamen[index];
                        baum = baum.previous;
                    }
                    index_gesucht++;
                    while (baum.previous != null)
                        baum = baum.previous;
                }
                else
                {
                    //int zurueck = 0, vorwaerts = 0, höhe = 0, temp = 2, temp2 = 2, rückzähler = 0;
                    int i = 0, t = 0, temp = 0;
                   // bool Toggle = true, rueck = false, gefunden = false, durchsucht = false;
                    bool[] Pfad = new bool[größe];
                    List<bool> Pfad2 = new List<bool>();
                    List<bool> Pfad1 = new List<bool>();
                    bool gefunden = false,Toggle = true;
                    größe++;
                    while (gefunden == false)
                    {
                        while (i != größe)
                        {
                            Pfad1.Add(false);
                            if (baum.left != null)
                            {
                                baum = baum.left;
                                if (index_gesucht == baum.Knotenindex)
                                {
                                    gefunden = true;
                                    break;
                                }
                            }
                            i++;
                            if (gefunden == false & baum.left == null)
                            {
                                baum = baum.previous;
                                Pfad1.RemoveAt(Pfad1.Count - 1);
                                baum = baum.right;
                                Pfad1.Add(true);
                                if (index_gesucht == baum.Knotenindex)
                                {
                                    gefunden = true;
                                    break;
                                }
                            }
                        }
                        while (Toggle == true)
                        { 
                            if(t<(Pfad1.Count))
                            t++;
                            if (Pfad1[Pfad1.Count - t] == true & Pfad1[0] == false)
                            {
                                if (t == Pfad1.Count - 1)
                                    break;
                            }
                            else { Toggle = false; }
                        }
                        while (Toggle == true)
                        {
                            if (baum.previous != null)
                            {
                                baum = baum.previous;
                                Pfad1.RemoveAt(Pfad1.Count - 1);
                            }
                            if (baum.previous == null)
                            {
                                baum = baum.right;
                                Pfad1.Add(true);
                                Toggle = false;
                            }
                        }
                        i = 0;
                        while (i != größe)
                        { 
                            if (baum.left != null)
                            {
                                Pfad1.Add(false);
                                baum = baum.left;
                                if (index_gesucht == baum.Knotenindex)
                                {
                                    gefunden = true;
                                    break;
                                }
                            }
                            i++;
                            if (gefunden == false & baum.left == null)
                            {
                                baum = baum.previous;
                                Pfad1.RemoveAt(Pfad1.Count - 1);
                                baum = baum.right;
                                Pfad1.Add(true);
                                if (index_gesucht == baum.Knotenindex)
                                {
                                    gefunden = true;
                                    break;
                                }
                            }
                        }



                    }
                }
            }
        }

        public void hash_Reihenfolge()
        { }
    }

    class hash
    {
        public String[] hash_Funktion(String[] Vornamen)
        {
            int K = 1;
            String[] Speicher = new String[32];
            int Summe_Kollisionen = 0;
            int[] Schlüssel = new int[20];
            int[] Kollisionen = new int[20];
            char[] name = new char[20];
            int zahl = 0;
            String[] vornamen = new String[32];
            for (int i = 0; i != 20; i++)       //Schlüssel berechnen
            {
                name = Vornamen[i].ToCharArray();
                for (int z = 0; z != 3; z++)
                {
                    if (Char.IsLower(name[z])) { zahl = 96; }
                    else if (Char.IsUpper(name[z])) { zahl = 64; }
                    Schlüssel[i] += ((int)name[z] - zahl);
                }
                Schlüssel[i] = (Schlüssel[i]+ i) % 32;
            }
            for (int i = 0; i != 20; i++)
            {
                if (Speicher[Schlüssel[i]] == null)
                {
                    Speicher[Schlüssel[i]] = Vornamen[i];
                }
                else
                {
                    K = 1;
                    bool eingefügt = false;
                    while (eingefügt == false)
                    {   

                        Kollisionen[i]++;
                        Summe_Kollisionen++;
                        if (Speicher[Schlüssel[i]+K] == null)
                        {
                            Speicher[Schlüssel[i]+K] = Vornamen[i];
                            eingefügt = true;
                        }
                        else
                        {
                            K++;
                        }
                    }
                    Schlüssel[i] = Schlüssel[i] + K;
                }
            }
            int index = 0;
            int kollisionenindex = 0;
                while (Speicher.Length != index)
                {
                    if (Speicher[index] != null)
                    {
                        System.Console.WriteLine("Vorname: " + Speicher[index] + " " + "Schlüssel: " + index + " " + "Kollisionen: " + Kollisionen[kollisionenindex]);
                        index++;
                        kollisionenindex++;
                    }
                    else index++;
                }
                System.Console.WriteLine("Summe aller Kollisionen "+Summe_Kollisionen);
            return vornamen;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            String[] Vornamen = new String[] { "Alexander", "David", "Felix", "Maximilian", "Leon", "Lukas", "Luca", "Paul", "Jonas", "Tim", "Anna", "Emily", "Julia", "Maria", "Laura", "Lea", "Lena", "Leonie", "Marie", "Sophie" };
            binärbaum baum = new binärbaum();
            System.Console.Write("");
            hash vornamen = new hash();
            vornamen.hash_Funktion(Vornamen);
            baum.normale_Reihenfolge(Vornamen);
            System.Console.Read();
        }
    }
}

