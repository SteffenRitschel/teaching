import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

//ich wei� nicht wie die folgenden Methoden genau arbeiten versuche aber trotzdem so gut wie m�glich diese zu beschreiben falls mein Beschreibung 
//falsch sein mag bitte ich denjenigen der es besser wei� als ich die Kommentierung zu verbessern

//Quuelle: http://algs4.cs.princeton.edu/55compression/Huffman.java.html



public class Huffman {
	Zeichen zeichen;
	Huffman previous;
	Huffman left;
	Huffman right;
	int sum;

	@SuppressWarnings("unused")
	public Huffman()
	{
		Huffman previous=null;
		Huffman left=null;
		Huffman right=null;
	}



	private boolean isLeaf() {		//pr�fen ob es ein baum ist
		assert (left == null && right == null) || (left != null && right != null);		//pr�fe ob left && right null oder nicht null sind
		return (left == null && right == null);
	}

	public static void buildCode(ArrayList<Code> st, Huffman x, String s,Code d) {
		if (!x.isLeaf()) {							//wenn es kein Blatt ist gehe nach links und schreibe dabei den Code bis er zum Blatt kommt 
			buildCode(st, x.left,  s + '1',d);		//dann f�ge erstellten Code in die Liste ein
			buildCode(st, x.right, s + '0',d);		// diese Zeile wird erst bearbeitet wenn er denn ersten Code erstellt hat dann besuche das Blatt rechts vom aktuellen
		}											//m�glich dadurch weil in 'x' referenced from die beiden Bl�tter und  der vorletzte Knoten enthalten sind
		else {										
			d= new Code();
			d.code=s;
			d.Buchstabe=x.zeichen.buchstabe;		
			st.add(d);								//schreiben den Code und denn dazugeh�rigen Buchstaben in eine Liste
		}
	}
	public void codierung(ArrayList<Code> Code) throws IOException{
		String Dateiname="KHR95_red.txt";

		OutputStreamWriter writer = new OutputStreamWriter(new FileOutputStream("codiert"),"UTF-16");		//Schreibstream �ffnen f�r "Datei" in UTF-8

		BufferedWriter out = new BufferedWriter(new FileWriter("codiert"));
		BufferedReader buff = new BufferedReader(new FileReader(Dateiname));
		int codiert=0;
		char Dateiinhalt = 0;

		while (codiert != -1) {												//solange Dateiende nicht erreicht wurde
			codiert = buff.read();											//Zeichenweise lesen 
			if (codiert != -1) {											//wenn Dateiende noch nicht erreicht;
				Dateiinhalt = (char) codiert;
				Dateiinhalt= Character.toUpperCase(Dateiinhalt);
				for(int i=0;i<Code.size();i++){
					if(String.valueOf(Dateiinhalt).equals(Code.get(i).Buchstabe)){
						//""
						//out.write("hallo");
						writer.write(Code.get(i).code);
						i=Code.size();
						// out.flush(); 
					}
				}
			}
		}
		out.close();
		buff.close();
		System.out.println("Datei codiert");
	}


	public void decodierung(ArrayList<Code> Code) throws IOException{
		InputStream inStream = new FileInputStream("codiert");	//Datei mit der XML
		BufferedReader input =  new BufferedReader(new InputStreamReader(inStream,"UTF-8"));		BufferedWriter out = new BufferedWriter(new FileWriter("decodiert"));
		int codiert=0;
		String Dateiinhalt = "";
		while (codiert != -1 ) {												//solange Dateiende nicht erreicht wurde
			codiert = input.read();												//Zeichenweise lesen 
			if (codiert != -1&& codiert<100&& codiert!=0) {											//wenn Dateiende noch nicht erreicht;
				codiert=codiert-48;
				Dateiinhalt +=  codiert;
			}
		
		while(Dateiinhalt.length()>=6){
			for(int i=0;i<Code.size();i++){
				if(Dateiinhalt.startsWith(Code.get(i).code)){
					out.write(Code.get(i).Buchstabe);
					//System.out.println(Code.get(i).Buchstabe);
					Dateiinhalt=Dateiinhalt.replaceFirst(Code.get(i).code, "");
					
					i=Code.size();	
				}
			}
		}	
	}
		//input.close();
				//inStream.close();
		System.out.println("Datei decodiert");
	}
}

